# Runtimes
This repository contains the source code for all Kotlin based runtimes used by Themis.

Themis is the university of Groningen's system for automating the grading of student's code submissions.
A runtime is responsible for execution the lowest unit of a submission, referred to as an "Action".

A runtime purely concerns itself with executing such an action, including, but not limited to:
- Executing the code and producing output
- Enforcing resource limits
- Isolation, containerisation, or sandboxing
- Gathering metrics about the execution

Notably, it is not aware of higher-level submission concepts like testcases, jobs, stages, or submissions.
It just sees a single action that it must execute.

## Container Runtime
The container runtime is responsible for executing untrusted, containerised workloads.
It places an emphasis on being able to safely handle any code as efficiently as possible.

The environment in which actions executed by the container runtime run is containerised and isolated from the host system.
This goes beyond the normal scope of a container, and includes Kernel-level isolation using gVisor.
