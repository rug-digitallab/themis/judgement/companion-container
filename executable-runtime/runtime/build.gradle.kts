plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project

    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusVersion")

    // Module dependencies
    implementation(project(":common-runtime"))
}
