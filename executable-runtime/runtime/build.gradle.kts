plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val commonQuarkusVersion: String by project

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusVersion")

    // Module dependencies
    implementation(project(":common-runtime"))
}
