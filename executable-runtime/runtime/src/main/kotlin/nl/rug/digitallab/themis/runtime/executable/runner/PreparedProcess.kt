package nl.rug.digitallab.themis.runtime.executable.runner

import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import java.nio.file.Path

/**
 * Starts a processing running a command with the following preparations made to the process:
 * - Apply resource limits
 *   - Memory limit: The maximum amount of memory the process can use.
 *   - Wall time limit: The maximum amount of time the process can run.
 * - Set the working directory of the process to the temporary directory.
 *
 * @param resourceLimits The resource limits to apply to the process.
 * @param command The command to run.
 * @param workingDirectory The working directory in which the process should be started.
 */
class PreparedProcess(
    val resourceLimits: ActionExecutionLimits,
    val command: List<String>,
    val workingDirectory: Path,
) {
    /**
     * Behaves similarly to [ProcessBuilder.start], but applies memory and time limits to the process.
     *
     * The following limits are applied by this function:
     * - Memory limit: The maximum amount of memory the process can use in KB.
     * - Wall time limit: The maximum amount of time the process can run in seconds.
     * - User processes limit: The maximum number of processes the process can spawn.
     *
     * @return The started process.
     */
    fun start(): Process {
        require(workingDirectory.toFile().exists()) {
            "The temporary working directory was not correctly created at: ${workingDirectory.toAbsolutePath()}"
        }

        // Extra verification check to ensure the working directory exists.
        val commandWithLimits = listOf(
            "prlimit", "-v=${resourceLimits.memory.B}",
            /*
             * Send SIGTERM after the wall time limit is reached. Then send SIGKILL 1 second later.
             * Once SIGTERM is triggered, it is impossible for the process to end with exit code 0.
             * It will always end with 124 (timeout) or 137 (SIGKILL).
             * So no shenanigans around ignoring SIGTERM and eventually succeeding is possible.
             *
             * -v enables verbose logging. This makes timeout print to stderr when it sends a signal.
             * This helps debugging around the action command itself sending exit code 124 or timeout sending exit code 137 with -k.
             */
            "timeout", "-k", "1s", "-v", "${resourceLimits.wallTime.inWholeNanoseconds / 1_000_000_000f}",
        ) + command

        return ProcessBuilder(commandWithLimits)
            // Set the working directory of the process to the temporary directory.
            .directory(workingDirectory.toFile())
            .start()
    }
}
