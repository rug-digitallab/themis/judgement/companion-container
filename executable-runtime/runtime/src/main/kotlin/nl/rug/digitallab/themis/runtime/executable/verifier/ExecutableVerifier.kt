package nl.rug.digitallab.themis.runtime.executable.verifier

import jakarta.enterprise.context.ApplicationScoped
import org.eclipse.microprofile.config.inject.ConfigProperty

/**
 * Verifies that an executable is allowed to be run by the runtime.
 *
 * This must always be used before running an executable to ensure that the executable is allowed to be run for security reasons.
 */
@ApplicationScoped
class ExecutableVerifier {
    @ConfigProperty(name = "digital-lab.executable-runtime.allowed-executables")
    private lateinit var allowedExecutables: List<String>

    /**
     * Verify that an executable is allowed to be run by the runtime.
     *
     * @param executable The name of the executable to verify.
     *
     * @return True if the executable is allowed to be run, false otherwise.
     */
    fun verifyExecutable(executable: String): Boolean {
        return allowedExecutables.contains(executable)
    }
}
