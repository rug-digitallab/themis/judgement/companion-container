package nl.rug.digitallab.themis.runtime.executable.runner

import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.executable.exceptions.OutputLimitException
import java.io.ByteArrayOutputStream
import java.io.InputStream

/**
 * Utility class for reading the output of a process.
 * Enforces a total limit on all output streams combined.
 *
 * @property cumulativeOutputLimit The maximum amount of output that can be read in total.
 */
class OutputReader(
    private var cumulativeOutputLimit: Long,
) {
    /**
     * Reads out a single stream while subtracting the number of bytes read from the cumulative limit provided in the constructor.
     * If a stream exceeds the limit, the function will report the status [ActionStatus.EXCEEDED_STREAM_OUTPUT] and return the data read so far.
     * This function will close the stream after reading.
     *
     * @param inputStream The stream to read from.
     *
     * @return The data read from the stream.
     *
     * @throws OutputLimitException If the cumulative output limit is exceeded by this stream.
     */
    fun readStreamWithLimit(inputStream: InputStream): ByteArray {
        val buffer = ByteArray(8192) // 8 KB buffer size
        val outputStream = ByteArrayOutputStream()

        inputStream.use { stream ->
            while (true) {
                val bytesRead = stream.read(buffer)
                if (bytesRead == -1) break
                cumulativeOutputLimit -= bytesRead

                // Check if we've exceeded the limit
                if (cumulativeOutputLimit < 0)
                    throw OutputLimitException()

                outputStream.write(buffer, 0, bytesRead)
            }


            return outputStream.toByteArray()
        }
    }
}
