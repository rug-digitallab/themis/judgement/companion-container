package nl.rug.digitallab.themis.runtime.executable.exceptions

/**
 * An exception that is thrown when the cumulative output limit of the output streams of a process is exceeded.
 */
class OutputLimitException : Exception("The stream's output limit has been exceeded.")
