package nl.rug.digitallab.themis.runtime.executable.runner

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.supervisorScope
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.runtime.common.runner.ActionRunner
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionExecutionMetrics
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionOutput
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionResult
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.executable.exceptions.OutputLimitException
import nl.rug.digitallab.themis.runtime.executable.verifier.ExecutableVerifier
import org.jboss.logging.Logger
import java.io.File
import java.nio.file.Path

/**
 * A runner that runs executables on the operating system of the container running this runtime.
 *
 * Note that this can be a security risk. Always validate the command before running it.
 *
 * @see nl.rug.digitallab.themis.runtime.executable.verifier.ExecutableVerifier
 */
@ApplicationScoped
class ExecutableRunner : ActionRunner {
    @Inject
    private lateinit var executableVerifier: ExecutableVerifier

    @Inject
    private lateinit var logger: Logger

    /**
     * Run the command specified in the request.
     *
     * @param configuration The configuration defining how to run the executable. This is the common configuration for all runtimes.
     *
     * @return The response to running the command.
     */
    override suspend fun runAction(
        configuration: ActionConfiguration
    ): ActionResult {
        // IMPORTANT: Always verify that the executable is within the list of allowed executables.
        require(executableVerifier.verifyExecutable(configuration.command.first())) {
            "Executable is not allowed to be run in the executable runtime: ${configuration.command.first()}"
        }

        // Create a temporary directory in which files used by the executable can be stored.
        // Will be deleted after the execution is done.
        withEphemeralDirectory { directory ->
            return runExecutable(configuration, directory)
        }
    }

    /**
     * Handles the execution of the executable to produce the result.
     *
     * @param configuration The configuration defining how to run the executable. This is the common configuration for all runtimes.
     * @param workingDirectory The path to the directory that should be the working directory for the executable. All files should be inserted here before running the executable.
     *                         It is the caller's responsibility to clean up this directory after the execution is done.
     *
     * @return The produced result from running the executable.
     */
    private suspend fun runExecutable(
        configuration: ActionConfiguration,
        workingDirectory: Path,
    ): ActionResult = withCoroutineSpan("RUN executable", Dispatchers.IO) {
        // Insert files provided by the request into the working directory.
        configuration.inputs.filter { it.uri.scheme == "file" }.forEach { insertFile(it, workingDirectory) }

        // Start the executable
        val process = PreparedProcess(
            command = configuration.command,
            resourceLimits = configuration.executionLimits,
            workingDirectory = workingDirectory,
        ).start()

        with(ActionContext(
            resourceId = process.pid().toString(),
            logger = logger,
            executionLimits = configuration.executionLimits,
        )) {
            // Asynchronously start monitoring stdout and stderr. This allows early collection of output.
            // It also allows for killing the process early if it exceeds the output limit.
            val outputJob = launch { monitorOutput(process) }
            // Wait for the process to finish
            exitCode = process.waitFor()
            // Wait for the output monitoring job to finish
            outputJob.join()

            when(exitCode) {
                // SUCCEEDED has the lowest priority, so if another status has already been set, it will not be overwritten.
                0 -> reportStatus(ActionStatus.SUCCEEDED)
                // Timeout will report 124 if the process timed out. Note that it will report 137 if the SIGTERM is ignored and timeout has to send SIGKILL.
                // The executed command should not throw 124 by convention, but can do it. Timeout will also report to stderr that the process was killed.
                124 -> reportStatus(ActionStatus.EXCEEDED_WALL_TIME)
                else -> reportStatus(ActionStatus.CRASHED)
            }

            val response = ActionResult(
                status = actionExecStatus,
                exitCode = exitCode,
                outputs = listOf(
                    ActionOutput.fromOutStream("stdout", stdout),
                    ActionOutput.fromOutStream("stderr", stderr),
                ),
                executionMetrics = ActionExecutionMetrics(),
            )

            return@withCoroutineSpan response
        }
    }

    /**
     * Inserts a single resource into the working directory.
     * The resource must be of type "file".
     *
     * @param resource The resource to insert into the working directory.
     * @param directory The directory in which to insert the file.
     */
    private fun insertFile(resource: ActionResource, directory: Path) {
        val file = File(
            directory.toFile(),
            resource.uri.schemeSpecificPart,
        )

        require(!file.exists()) {
            "Resource with URI ${resource.uri} already exists in the working directory. Do two resources have the same URI?"
        }

        file.writeBytes(resource.content)
    }

    /**
     * Monitor the output of the process and store it in the context.
     * This will run asynchronously, and should be started after starting the process.
     * It can detect excessive output and kill the process if it exceeds the limit.
     *
     * It makes use of a supervisorScope in order to catch exceptions thrown by the OutputReader.
     *
     * @param process The process to monitor the output of.
     */
    context(ActionContext)
    private suspend fun monitorOutput(process: Process) = supervisorScope {
        val outputReader = OutputReader(executionLimits.output.B)

        try {
            // The Process API names the streams from the other side of the pipe than what you may expect.
            // The `inputStream` is in fact the output stream from the perspective of the process.
            // The reason it is called `inputStream` is that it is read as
            // an input stream from the perspective of the Java application.
            val stdoutDeferred = async { outputReader.readStreamWithLimit(process.inputStream) }
            val stderrDeferred = async { outputReader.readStreamWithLimit(process.errorStream) }

            // Wait for both jobs to finish
            stdout = stdoutDeferred.await()
            stderr = stderrDeferred.await()
        } catch (_: OutputLimitException) {
            reportStatus(ActionStatus.EXCEEDED_STREAM_OUTPUT)
            process.destroyForcibly()
        }
    }
}
