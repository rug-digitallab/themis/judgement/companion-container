package nl.rug.digitallab.themis.runtime.executable.runner

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import kotlin.io.path.Path
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class PreparedProcessTest {
    @Inject
    private lateinit var limitsConfig: ResourceLimitsConfig

    @Test
    fun `The shell applies a memory limit that crashes the process if it exceeds it`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 1.KB,
                cpuCores = 1f,
                wallTime = 5.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 5,
                actionProcesses = 50,
                limitsConfig = limitsConfig
            ),
            // Run a command that will consume more than 1KB of memory
            command = listOf("dd", "if=/dev/zero", "of=/dev/null", "bs=1K", "count=2"),
            workingDirectory = Path("."),
        ).start()

        // The exit code of an OOM killed process can differ between environments, so we only check if it is not 0
        assertNotEquals(0, process.waitFor())
    }

    @Test
    fun `The shell applies a memory limit that does not crash the process if it does not exceed it`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 5.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            // Run a command that will consume more than 1KB of memory
            command = listOf("dd", "if=/dev/zero", "of=/dev/null", "bs=1K", "count=2"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
    }

    @Test
    fun `The shell can handle a reasonable command with reasonable limits`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 5.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 100,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("echo", "Hello, World!"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
        assertEquals("Hello, World!\n", process.inputStream.readBytes().toString(Charsets.UTF_8))
        assertEquals("", process.errorStream.readBytes().toString(Charsets.UTF_8))
    }

    @Test
    fun `A process can time out if it takes too long`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 1.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("sleep", "10"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(124, process.waitFor())
    }

    @Test
    fun `Fractional time is supported`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 100.milliseconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("sleep", "1"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(124, process.waitFor())
    }

    @Test
    fun `Command injection should not be possible on the shell`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 1.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("echo", "hello ; exit 5", "||", "exit", "5"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
        // Test that the attempted shell injection is instead passed as an argument to echo
        assertEquals(
            "hello ; exit 5 || exit 5\n",
            process.inputStream.readBytes().toString(Charsets.UTF_8),
        )
    }


    @Test
    fun `Command injection prevention can deal with a quote in the command`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 1.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("echo", "hello 'world'"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
        assertEquals(
            "hello 'world'\n",
            process.inputStream.readBytes().toString(Charsets.UTF_8),
        )
    }

    @Test
    fun `Quote marks cannot be used to do command injection`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 1.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig
            ),
            command = listOf("echo", "hello world' ; exit 5", "'", "||", "exit", "5"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
        assertEquals(
            "hello world' ; exit 5 ' || exit 5\n",
            process.inputStream.readBytes().toString(Charsets.UTF_8),
        )
    }

    @Test
    fun `Diff can be run as a command`() {
        val process = PreparedProcess(
            resourceLimits = ActionExecutionLimits(
                memory = 100.MB,
                cpuCores = 1f,
                wallTime = 1.seconds,
                disk = 100.MB,
                output = 100.MB,
                userProcesses = 200,
                actionProcesses = 100,
                limitsConfig = limitsConfig,
            ),
            command = listOf("diff", "/dev/null", "/dev/null"),
            workingDirectory = Path("."),
        ).start()

        assertEquals(0, process.waitFor())
    }

    @Test
    fun `The PreparedProcess tests that the working directory actually exists`() {
        assertThrows<IllegalArgumentException> {
            PreparedProcess(
                resourceLimits = ActionExecutionLimits(
                    memory = 100.MB,
                    cpuCores = 1f,
                    wallTime = 1.seconds,
                    disk = 100.MB,
                    output = 100.MB,
                    userProcesses = 200,
                    actionProcesses = 100,
                    limitsConfig = limitsConfig
                ),
                command = listOf("echo", "Hello, World!"),
                workingDirectory = Path("nonexistent"),
            ).start()
        }
    }
}
