package nl.rug.digitallab.themis.runtime.executable.verifier

import io.quarkus.test.junit.QuarkusTest
import io.smallrye.common.constraint.Assert.assertFalse
import io.smallrye.common.constraint.Assert.assertTrue
import jakarta.inject.Inject
import org.junit.jupiter.api.Test

@QuarkusTest
class ExecutableVerifierTest {
    @Inject
    private lateinit var verifier: ExecutableVerifier

    @Test
    fun `A disallowed executable gets blocked by the verifier`() {
        val allowed = verifier.verifyExecutable("exec")
        assertFalse(allowed)
    }

    @Test
    fun `An allowed executable gets allowed by the verifier`() {
        val allowed = verifier.verifyExecutable("diff")
        assertTrue(allowed)
    }
}
