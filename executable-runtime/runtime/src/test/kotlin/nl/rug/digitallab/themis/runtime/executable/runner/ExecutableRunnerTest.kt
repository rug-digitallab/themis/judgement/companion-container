package nl.rug.digitallab.themis.runtime.executable.runner

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourceMetadata
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionFeatures
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.net.URI
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class ExecutableRunnerTest {
    @Inject
    private lateinit var runner: ExecutableRunner

    @Inject
    private lateinit var limitsConfig: ResourceLimitsConfig

    @Test
    fun `The action is marked succeeded when no issues arrise`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "Hello, World!"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.SUCCEEDED,
            result.status,
        )
    }

    @Test
    fun `The executable runner can execute a basic echo command`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "Hello, World!"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            "Hello, World!\n",
            result.outputs[0].resource.content.toString(Charsets.UTF_8)
        )
    }

    @Test
    fun `The executable runner can execute an echo command split on multiple parameters`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "Hello,", "World!"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 10.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            "Hello, World!\n",
            result.outputs[0].resource.content.toString(Charsets.UTF_8)
        )
    }

    @Test
    fun `The executable runner handles empty command parameters gracefully`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", ""),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            "\n",
            result.outputs[0].resource.content.toString(Charsets.UTF_8)
        )
    }

    @Test
    fun `The executable runner verifies the command before running it`() = runBlocking {
        assertThrows<IllegalArgumentException> {
            runner.runAction(
                ActionConfiguration(
                    image = "",
                    command = listOf("exec"),
                    envVars = emptyMap(),
                    inputs = emptyList(),
                    executionLimits = ActionExecutionLimits(
                        wallTime = 5.seconds,
                        cpuCores = 1f,
                        actionProcesses = 50,
                        userProcesses = 5,
                        memory = 100.MB,
                        disk = 100.MB,
                        output = 100.MB,
                        limitsConfig = limitsConfig,
                    ),
                    outputRequests = emptyList(),
                    features = ActionFeatures(
                        publicNetworkAccess = true,
                    ),
                )
            )
        }

        return@runBlocking
    }

    @Test
    fun `The status is marked as crashed if the command returns a non-zero exit code`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("exit", "1"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.CRASHED,
            result.status,
        )
    }

    @Test
    fun `The executable runner properly enforces the output limit`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "Hello World!"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 10.B,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.EXCEEDED_STREAM_OUTPUT,
            result.status,
        )
    }

    @Test
    fun `The output is not exceeded when there is barely enough room left`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "Hello World!"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.B,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.SUCCEEDED,
            result.status,
        )
    }

    @Test
    fun `Shell injection does not work on the executable runner`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("echo", "hello ; exit 5", "||", "exit", "5"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        println(result.outputs[0].resource.content.toString(Charsets.UTF_8))

        assertEquals(
            0,
            result.exitCode,
        )

        assertEquals(
            "hello ; exit 5 || exit 5\n",
            result.outputs[0].resource.content.toString(Charsets.UTF_8)
        )
    }

    @Test
    fun `Stderr is separated from stdout`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("ls", "/nonexistent"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.MB,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            "",
            result.outputs.find{ it -> it.resource.uri == URI("stream:stdout") }!!.resource.content.toString(Charsets.UTF_8)
        )

        assertEquals(
            "ls: cannot access '/nonexistent': No such file or directory\n",
            result.outputs.find{ it -> it.resource.uri == URI("stream:stderr") }!!.resource.content.toString(Charsets.UTF_8)
        )
    }

    @Test
    fun `A program can time out if it takes too long`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("sleep", "10"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 10.milliseconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.B,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.EXCEEDED_WALL_TIME,
            result.status,
        )
    }

    @Test
    fun `A program that produces infinite output gets terminated`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("yes"),
                envVars = emptyMap(),
                inputs = emptyList(),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.B,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.EXCEEDED_STREAM_OUTPUT,
            result.status,
        )
    }

    @Test
    fun `The executable runtime receives two resources with the same URI`() = runBlocking {
        assertThrows<IllegalArgumentException> {
            runner.runAction(
                ActionConfiguration(
                    image = "",
                    command = listOf("echo", "Hello, World!"),
                    envVars = emptyMap(),
                    inputs = listOf(
                        ActionResource(
                            URI("file:1.txt"),
                            "Hello, World!".toByteArray(Charsets.UTF_8),
                            metadata = ActionResourceMetadata(0, 0, 777),
                        ),
                        ActionResource(
                            URI("file:1.txt"),
                            "Hello, World!".toByteArray(Charsets.UTF_8),
                            metadata = ActionResourceMetadata(0, 0, 777),
                        ),
                    ),
                    executionLimits = ActionExecutionLimits(
                        wallTime = 5.seconds,
                        cpuCores = 1f,
                        actionProcesses = 50,
                        userProcesses = 5,
                        memory = 100.MB,
                        disk = 100.MB,
                        output = 100.B,
                        limitsConfig = limitsConfig,
                    ),
                    outputRequests = emptyList(),
                    features = ActionFeatures(
                        publicNetworkAccess = true,
                    ),
                )
            )
        }

        return@runBlocking
    }

    @Test
    fun `The executable runtime can run a diff command`() = runBlocking {
        val result = runner.runAction(
            ActionConfiguration(
                image = "",
                command = listOf("diff", "1.txt", "2.txt"),
                envVars = emptyMap(),
                inputs = listOf(
                    ActionResource(
                        URI("file:1.txt"),
                        "Hello, World!".toByteArray(Charsets.UTF_8),
                        metadata = ActionResourceMetadata(0, 0, 777),
                    ),
                    ActionResource(
                        URI("file:2.txt"),
                        "Hello, World!".toByteArray(Charsets.UTF_8),
                        metadata = ActionResourceMetadata(0, 0, 777),
                    ),
                ),
                executionLimits = ActionExecutionLimits(
                    wallTime = 5.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 100.MB,
                    disk = 100.MB,
                    output = 100.B,
                    limitsConfig = limitsConfig,
                ),
                outputRequests = emptyList(),
                features = ActionFeatures(
                    publicNetworkAccess = true,
                ),
            )
        )

        assertEquals(
            ActionStatus.SUCCEEDED,
            result.status,
        )
    }
}
