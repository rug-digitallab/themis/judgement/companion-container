package nl.rug.digitallab.themis.runtime.executable.runner

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.executable.exceptions.OutputLimitException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import java.io.ByteArrayInputStream

@QuarkusTest
class OutputReaderTest {
    @Test
    fun `The stream is not fully read if it exceeds the output limit to prevent excessive resource usage`() {
        // Create a stream that holds more than 10 KB of data
        val stream = ByteArrayInputStream("Hello, World!".repeat(1024).toByteArray())

        val outputReader = OutputReader(cumulativeOutputLimit = 10.KB.B)
        assertThrows<OutputLimitException> {
            outputReader.readStreamWithLimit(stream)
        }
    }

    @Test
    fun `The stream is fully read if it does not exceed the output limit`() {
        // Create a stream that holds less than 10 MB of data
        val stream = ByteArrayInputStream("Hello, World!".repeat(1024).toByteArray())

        val outputReader = OutputReader(cumulativeOutputLimit = 10.MB.B)

        assertDoesNotThrow {
            val result = outputReader.readStreamWithLimit(stream)

            assertEquals(
                "Hello, World!".repeat(1024),
                result.toString(Charsets.UTF_8)
            )
        }
    }
}
