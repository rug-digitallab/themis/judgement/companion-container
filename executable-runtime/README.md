# Executable Runtime
The executable runtime is responsible for running trusted uncontainerised workloads.
In a generalised context, this service receives gRPC function calls to run action.
The executable runtime will run these actions as processes within the container the executable runtime is running, and return the results along with execution metrics as the function's result.
In a Themis-specific context, this service will run the trusted workloads such as diffing the output of the student code with the expected output.

This service places an emphasis on trusted workloads.
Requests made to this service must never include malicious programs.
The runtime has basic security measures such as resource limits on the process and containerisation of the whole service.
The runtime also only allows certain safe executables to be run, such as `diff` and `echo`.
This should prevent arbitrary executables from being run on the runtime, which would not be a safe operation.

## Supported features from the communication spec
This section describes the features of the communication spec that are supported by this runtime.
Some of these features may still need to be implemented, some others may not be possible to implement due to the nature of the runtime.

### Request
- [x] Resource limits
    - [x] Walltime (Rounded to whole seconds, with a minimum of 1 second)
    - [ ] CPU cores (fractional)
    - [x] Memory (Rounded to KB)
    - [ ] Disk space
    - [ ] Action processes
    - [ ] User processes
    - [x] Output size
- [ ] Environment (Will not be supported, as the action is a native process)
- [x] Startup command
- [ ] Environment variables
- [x] Input files
- [ ] Requested output files
- [ ] Stdin
- [ ] Features
    - [ ] Zombie/Orphan monitor
    - [ ] Disable/Enable public network access (Currently always enabled)

### Response
- [x] Exit code
- [x] Status
    - [x] SUCCEEDED
    - [x] CRASHED
    - [x] EXCEEDED_WALL_TIME
    - [ ] EXCEEDED_MEMORY
    - [x] EXCEEDED_STREAM_OUTPUT
    - [ ] EXCEEDED_OUTPUT_FILE
    - [ ] EXCEEDED_PROCESSES (Only for user processes exceeded)
    - [ ] PRODUCED_ORPHANS
    - [ ] PRODUCED_ZOMBIES
    - [ ] PRODUCED_ZOMBIES_AND_ORPHANS
- [x] Stdout stream
- [x] Stderr stream
- [ ] Output files
- [ ] Resource usage
    - [ ] Walltime
    - [ ] Memory
    - [ ] Disk space used
    - [ ] Output size
    - [ ] Created processes
    - [ ] Max processes
    - [ ] Orphaned processes
    - [ ] Zombie processes
    - [ ] IO time
    - [ ] CPU time
