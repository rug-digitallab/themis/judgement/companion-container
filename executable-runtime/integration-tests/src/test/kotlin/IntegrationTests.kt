import io.quarkus.test.common.QuarkusTestResource
import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.executable.integration.environments.ExecutableIntegrationEnvironmentSpawner
import nl.rug.digitallab.themis.runtime.integration.client.junit.IntegrationTestGenerator

@QuarkusTest
@QuarkusTestResource(value = ExecutableIntegrationEnvironmentSpawner::class)
class IntegrationTests {
    @Inject
    private lateinit var generator: IntegrationTestGenerator

    /**
     * Dynamically generates unit tests executed by JUnit
     * This will generate 1 JUnit test per integration test.
     *
     * @return A list of dynamic tests to run.
     */
    @TestFactory
    fun spawnIntegrationTests(): List<DynamicTest> = generator.generateIntegrationTests()
}
