package nl.rug.digitallab.themis.runtime.executable.integration.environments

import nl.rug.digitallab.themis.runtime.integration.client.environment.BaseIntegrationEnvironment
import org.slf4j.LoggerFactory
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.images.builder.ImageFromDockerfile
import org.testcontainers.utility.MountableFile
import java.time.Duration

/**
 * A basic environment for the executable runtime.
 * It essentially is a normal container that hosts the executable runtime as it would be in production.
 */
class NativeIntegrationEnvironment : BaseIntegrationEnvironment {
    // The port the gRPC server is running on
    // This is only used internally within the container. Testcontainers will automatically map this to a random external port.
    // You can find the external port by calling `container.getMappedPort(internalPort)`.
    private val internalPort = 9000

    private val container = GenericContainer(
            ImageFromDockerfile()
                .withDockerfileFromBuilder { builder ->
                    builder.from("registry.gitlab.com/rug-digitallab/resources/container-images/jvm-runtime:latest")
                        .user("root")
                        // The executable runtime needs access to prlimit, which is in the util-linux package.
                        .run("microdnf update -y && microdnf install -y util-linux diffutils")
                        .user("185")
                        .build()
                }
        )
        .withExposedPorts(internalPort)
        .withCreateContainerCmdModifier { cmd ->
            cmd.withUser("root")
        }
        .withCopyFileToContainer(
            MountableFile.forHostPath("../runtime/build/quarkus-app"),
            "/deployments",
        )
        .waitingFor(Wait.defaultWaitStrategy().withStartupTimeout(Duration.ofMinutes(10)))

    /**
     * Start the container and return the host and port of the gRPC server.
     *
     * @return A map containing the host and port of the gRPC server.
     */
    override fun start(): Map<String, String> {
        container.start()

        // Write the logs of the executable runtime to the test logs.
        // Unfortunately this functionality in testcontainers is specific to SLF4J,
        // so we have to use that.
        container.followOutput(
            Slf4jLogConsumer(LoggerFactory.getLogger(NativeIntegrationEnvironment::class.java))
        )

        return mapOf(
            "digital-lab.integration-tester.grpc.host" to container.host,
            "digital-lab.integration-tester.grpc.port" to container.getMappedPort(internalPort).toString(),
        )
    }

    /**
     * Stop the container.
     */
    override fun stop() {
        container.stop()
    }
}
