package nl.rug.digitallab.themis.runtime.executable.integration.environments

import nl.rug.digitallab.themis.runtime.integration.client.environment.BaseIntegrationEnvironment
import nl.rug.digitallab.themis.runtime.integration.client.environment.IntegrationEnvironmentSpawner

/**
 * Since a Quarkus test resource cannot take constructor arguments, we need pass the environments using inheritance.
 * The logic of actually spawning the integration environment is in the parent class.
 */
class ExecutableIntegrationEnvironmentSpawner : IntegrationEnvironmentSpawner() {
    override val environments: Map<String, BaseIntegrationEnvironment> = mapOf(
        "native" to NativeIntegrationEnvironment(),
    )
}
