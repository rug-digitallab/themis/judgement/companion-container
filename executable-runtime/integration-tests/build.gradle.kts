plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val testcontainersVersion: String by project

    implementation(project(":integration-client"))

    implementation("org.testcontainers:testcontainers:$testcontainersVersion")
    implementation("io.quarkus:quarkus-test-common")

    testImplementation("org.testcontainers:junit-jupiter:$testcontainersVersion")
}

tasks.named("test") {
    dependsOn(":executable-runtime:runtime:assemble")
}
