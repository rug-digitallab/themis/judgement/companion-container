# This script is the entrypoint for the testing environment.
# It will load and spin up the container runtime and integration tester.

# Fail on any error to ensure that errors in the environment construction are not ignored.
# Otherwise, these errors would be caught at the first integration test, which is less informative.
set -e

# Finish mounting the XFS filesystem with privileged access
mount -o pquota -t xfs /xfs.bin /var/lib/docker

# Start the docker daemon with the gVisor runtime
dockerd --add-runtime runsc=/usr/local/bin/runsc &

# Wait for the docker daemon to have started
while (! docker stats --no-stream ); do
  sleep 1
done

# Start the integration testing environment
docker load -i /base-image.tar

exec "$@"
