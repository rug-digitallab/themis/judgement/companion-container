package nl.rug.digitallab.themis.runtime.container.integration.environments

import nl.rug.digitallab.themis.runtime.integration.client.environment.BaseIntegrationEnvironment
import org.slf4j.LoggerFactory
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.images.builder.ImageFromDockerfile
import org.testcontainers.utility.MountableFile
import java.time.Duration
import kotlin.io.path.Path

/**
 * The containerised integration environment is an environment that fully implements the infrastructure for the container runtime.
 * The container runtime has complex infrastructure requirements, such as specific filesystems, gVisor, docker daemon configuration, etc.
 * Starting this resource as a container implements all of this so the user does not need to locally set up their environment.
 *
 * This environment is however slower than a host-based environment, as it uses Docker-in-Docker,
 * and its performance is not representative of a production environment. See [NativeIntegrationEnvironment] for a higher performance alternative.
 *
 * The way it is implemented is a container representing the host environment,
 * with the container runtime running as a nested container inside it. The environment exposes a port, which is forwarded to the nested container.
 *
 * This starts the integration environment as a container that runs during the `integration-tester` tests.
 * The environment is managed using TestContainers, however the lifecycle management of testcontainers does not integrate well with Quarkus.
 * As such, the testcontainer is wrapped in a QuarkusTestResourceLifecycleManager. This makes Quarkus manage the lifecycle of the container.
 */
class ContainerisedIntegrationEnvironment : BaseIntegrationEnvironment {
    // The port the gRPC server is running on
    // This is only used internally within the container. Testcontainers will automatically map this to a random external port.
    // You can find the external port by calling `container.getMappedPort(internalPort)`.
    private val internalPort = 9000

    private val container = GenericContainer(
        ImageFromDockerfile()
            // Add components of the integration test environment like the compose file and the entrypoint script
            .withFileFromPath("/integration-tests/environment", Path("environment"))
            // Add config files to the context for things like dockerd and gVisor configuration
            .withFileFromPath("/config", Path("../config"))
            // Add the dockerfile definition of the integration test environment image
            .withFileFromPath("Dockerfile", Path("environment/Dockerfile"))
        )
        .withPrivilegedMode(true)
        .withExposedPorts(internalPort)
        .withCopyFileToContainer(
            MountableFile.forHostPath("../runtime/build/quarkus-app"),
            // This directory will also be used by GitLab CI to copy the Quarkus app into the container runtime
            // For the CI integration tests. Since the local integration environment does not care about where to mount
            // This directory, we can just use the same directory as the CI tests.
            "/builds/container-runtime/runtime/build/quarkus-app",
        )
        .withCommand("docker", "compose", "--project-name", "integration", "up", "--abort-on-container-exit")
        .waitingFor(Wait.defaultWaitStrategy().withStartupTimeout(Duration.ofMinutes(10)))

    override fun start(): Map<String, String> {
        container.start()

        // Write the logs of the container runtime to the test logs.
        // Unfortunately this functionality in testcontainers is specific to SLF4J,
        // so we have to use that.
        container.followOutput(
            Slf4jLogConsumer(LoggerFactory.getLogger(ContainerisedIntegrationEnvironment::class.java))
        )

        // Load the dynamic host/port into the Quarkus config so other code can access it.
        return mapOf(
            "digital-lab.integration-tester.grpc.host" to container.host,
            "digital-lab.integration-tester.grpc.port" to container.getMappedPort(internalPort).toString()
        )
    }

    override fun stop() {
        container.stop()
    }
}
