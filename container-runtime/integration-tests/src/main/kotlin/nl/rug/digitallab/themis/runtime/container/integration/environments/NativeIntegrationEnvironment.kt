package nl.rug.digitallab.themis.runtime.container.integration.environments

import com.github.dockerjava.api.model.Bind
import com.github.dockerjava.api.model.Volume
import nl.rug.digitallab.themis.runtime.integration.client.environment.BaseIntegrationEnvironment
import org.slf4j.LoggerFactory
import org.testcontainers.containers.GenericContainer
import org.testcontainers.containers.output.Slf4jLogConsumer
import org.testcontainers.containers.wait.strategy.Wait
import org.testcontainers.utility.DockerImageName
import org.testcontainers.utility.MountableFile
import java.time.Duration

/**
 * The native integration environment simply starts a container running the container runtime.
 * Using this environment means the host machine must be set up to run the container runtime,
 * as the container runtime will talk to the host docker daemon, which must be properly configured.
 *
 * This environment is representative of production environments and is faster than the [ContainerisedIntegrationEnvironment].
 * However, it can be complex to set up the host machine to run the container runtime. If you want to quickly verify the runtime,
 * use the [ContainerisedIntegrationEnvironment] instead.
 *
 * This starts the integration environment as a container that runs during the `integration-tester` tests.
 * The environment is managed using TestContainers, however the lifecycle management of testcontainers does not integrate well with Quarkus.
 * As such, the testcontainer is wrapped in a QuarkusTestResourceLifecycleManager. This makes Quarkus manage the lifecycle of the container.
 */
class NativeIntegrationEnvironment : BaseIntegrationEnvironment {
    // The port the gRPC server is running on
    // This is only used internally within the container. Testcontainers will automatically map this to a random external port.
    // You can find the external port by calling `container.getMappedPort(internalPort)`.
    private val internalPort = 9000

    private val container = GenericContainer(
            DockerImageName.parse("registry.gitlab.com/rug-digitallab/resources/container-images/jvm-runtime:latest")
        )
        .withExposedPorts(internalPort)
        .withPrivilegedMode(true)
        .withCreateContainerCmdModifier { cmd ->
            cmd.withUser("root")
            cmd.hostConfig?.setBinds(
                Bind("/var/run/docker.sock", Volume("/var/run/docker.sock")),
                Bind("/sys/fs/cgroup/docker", Volume("/docker/cgroups")),
                Bind("/tmp/themis", Volume("/tmp/themis")),
            )
        }
        .withCopyFileToContainer(
            MountableFile.forHostPath("../runtime/build/quarkus-app"),
            "/deployments",
        )
        .waitingFor(Wait.defaultWaitStrategy().withStartupTimeout(Duration.ofMinutes(10)))

    override fun start(): Map<String, String> {
        container.start()

        // Write the logs of the container runtime to the test logs.
        // Unfortunately this functionality in testcontainers is specific to SLF4J,
        // so we have to use that.
        container.followOutput(
            Slf4jLogConsumer(LoggerFactory.getLogger(NativeIntegrationEnvironment::class.java))
        )

        return mapOf(
            "digital-lab.integration-tester.grpc.host" to container.host,
            "digital-lab.integration-tester.grpc.port" to container.getMappedPort(internalPort).toString(),
        )
    }

    override fun stop() {
        container.stop()
    }
}
