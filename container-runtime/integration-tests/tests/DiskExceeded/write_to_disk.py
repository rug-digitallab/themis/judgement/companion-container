import os

# Function to write 10MB of data to disk
def write_data_to_disk(filename):
    data = b'0' * 10 * 1024 * 1024  # 10MB of data
    try:
        with open(filename, 'wb') as file:
            file.write(data)
        print("Data written successfully. This shouldn't have happened because the container should have run out of memory.")
    except OSError as e:
        print("Error: There isn't enough space on disk to write the data.")
        exit(1)

if __name__ == "__main__":
    write_data_to_disk("test_data.bin")
