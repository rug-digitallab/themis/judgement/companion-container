plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val dockerJavaVersion: String by project
    val commonQuarkusVersion: String by project
    val otelKotlinVersion: String by project
    val kotlinMockitoVersion: String by project
    val instancioVersion: String by project
    val coroutineTestVersion: String by project
    val kotlinDatetimeVersion: String by project

    // Docker dependencies
    implementation("com.github.docker-java:docker-java:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-httpclient5:$dockerJavaVersion")
    implementation("com.github.docker-java:docker-java-transport-netty:$dockerJavaVersion")

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusVersion")
    implementation("nl.rug.digitallab.common.quarkus:archiver:$commonQuarkusVersion")

    // Module dependencies
    implementation(project(":container-runtime:process-monitor"))
    implementation(project(":common-runtime"))

    // Observability
    implementation("io.quarkus:quarkus-observability-devservices-lgtm")

    // Miscellaneous dependencies
    implementation("io.opentelemetry:opentelemetry-extension-kotlin:$otelKotlinVersion")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:$kotlinDatetimeVersion")

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("org.instancio:instancio-core:$instancioVersion")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutineTestVersion")
}
