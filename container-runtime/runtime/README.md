# Container Runtime
This module implements the gRPC server listening for requests to run containers for student code.
Functionality includes:
- gRPC endpoint for running containers
- Startup and management of the container using docker commands.
- Connection to the process monitor to retrieve data extracted from the container.
