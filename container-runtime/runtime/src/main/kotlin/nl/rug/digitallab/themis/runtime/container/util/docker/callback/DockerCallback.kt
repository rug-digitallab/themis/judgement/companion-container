package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import com.github.dockerjava.api.async.ResultCallbackTemplate
import org.jboss.logging.Logger

/**
 * A callback to process the output of generic docker commands.
 * Special handling of non-error messages are implemented by passing a message processor.
 * This should be used when you need to perform special processing on the output of the command.
 * If you simply want to log the output, use [GenericCallback] instead.
 *
 * @param T The type of the response.
 * @property logger The logger to use for logging messages received from the docker client.
 * @property messageProcessor The function to process the message passed over the information channel.
 */
open class DockerCallback<T>(
    private val logger: Logger,
    private val messageProcessor: (T) -> Unit,
) : ResultCallbackTemplate<DockerCallback<T>, T>() {
    override fun onNext(`object`: T) = messageProcessor(`object`)

    /**
     * Called whenever an error occurs.
     *
     * @param throwable The error that occurred.
     */
    override fun onError(throwable: Throwable?) {
        logger.error("Callback produced error: \"${throwable?.message}\"")

        onComplete()
    }
}
