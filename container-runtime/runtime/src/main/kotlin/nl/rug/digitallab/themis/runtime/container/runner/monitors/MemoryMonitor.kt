package nl.rug.digitallab.themis.runtime.container.runner.monitors

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.delay
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.DurationLongConverter
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import org.jboss.logging.Logger
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import kotlin.time.Duration

/**
 * A monitor that tracks the peak memory usage of a container.`
 * It does so through regularly sampling the `memory.peak` metric of the container's cgroup.
 * This metric tracks the highest memory usage so far.
 */
@ApplicationScoped
class MemoryMonitor : Monitor {
    @Inject
    private lateinit var config: CGroupConfig

    @Inject
    private lateinit var logger: Logger

    /**
     * Starts the monitor and tracks the peak memory usage of the container.
     */
    context(ActionContext)
    override suspend fun startMonitor() {
        val peakMemoryUsageFile = File("${config.baseDirectory()}/$resourceId/memory.peak")

        // The cgroup gets deleted when the container stops, so when the file is gone, the monitor can stop.
        while(true) {
            // We do not check if the file exists in the while loop because it may be deleted after the check and before
            // reading it, resulting in a classic TOC/TOU issue. Synchronization does not help either, as the file is
            // deleted outside the JVM.
            try {
                // Since the metric already takes the maximum value, we can just read it and store it.
                executionMetrics.memory = peakMemoryUsageFile.readText().trim().toLong().B
            } catch (e: IOException) {
                // If the container has stopped we can either expect a FileNotFoundException or an IOException with the
                // message "No such device". Unfortunately, there is no specific exception for the latter, and we don't
                // want to blindly catch and ignore all IOExceptions.
                if (e is FileNotFoundException || e.message == "No such device") {
                    logger.debug("The peak memory usage file was not found, stopping the monitor.", e)
                    break
                } else {
                    logger.error("An error occurred while reading the peak memory usage file: ${e.message}", e)
                    throw e
                }
            }

            // Time between samples
            delay(config.sampleDelay())
        }
    }
}

/**
 * Describes how to interact with the cgroup metrics
 *
 * @property baseDirectory The base directory of all the cgroups within the system.
 *                         Must point to a cgroupv2 directory with the cgroupfs driver.
 * @property sampleDelay The delay between samples of the cgroup metrics.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.cgroup")
interface CGroupConfig {
    fun baseDirectory(): String
    @WithConverter(DurationLongConverter::class)
    @WithName("sample-delay")
    fun sampleDelay(): Duration
}
