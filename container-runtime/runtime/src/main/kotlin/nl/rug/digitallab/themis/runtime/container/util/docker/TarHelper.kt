package nl.rug.digitallab.themis.runtime.container.util.docker

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import nl.rug.digitallab.themis.runtime.container.exceptions.FileTooLargeException
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourceMetadata
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream
import java.io.ByteArrayInputStream
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.io.OutputStream
import java.net.URI

/**
 * This class helps with creating tar archives for copying files into the container.
 */
@ApplicationScoped
class TarHelper {
    @Inject
    private lateinit var tarArchiver: TarArchiver

    /**
     * Creates a tar archive input stream so the container file can be copied into the container.
     * This function closes the output stream after it is done.
     *
     * @param actionResources The list of files to insert into the tar archive.
     * This allows a container unpacking the tar archive to insert all the files into the container.
     * @param outputStream The output stream to write the tar archive to.
     *
     * @return The tar archive input stream.
     */
    fun insertFilesIntoStreamAsTar(
        actionResources: List<ActionResource>,
        outputStream: OutputStream,
    ) {
        // Create the stream to write the tar file to, will be passed into the provided output stream
        tarArchiver.createArchiveOutputStream(outputStream).use { tarArchiveOutputStream ->
            actionResources.forEach { file -> insertFile(file, tarArchiveOutputStream) }
        }
    }

    /**
     * Extracts a [ActionResource] from a [TarArchiveInputStream].
     *
     * @param inputStream The tar archive input stream to extract the file from.
     * @param path The path of the file to extract. Only used for creating the name of the container file.
     * @param maxSize The maximum size of the file to extract.
     *
     * @return The extracted container file.
     */
    fun extractFileFromTarStream(
        inputStream: InputStream,
        path: String,
        maxSize: ByteSize,
    ): ActionResource {
        val tarArchiveInputStream = tarArchiver.createArchiveInputStream(inputStream)

        // Retrieve the tar header containing the metadata of the file
        val tarEntry = checkNotNull(tarArchiveInputStream.nextEntry) {
            "The header of a retrieved container file is null"
        }

        // Verify that the file is not too large
        if(tarEntry.size > maxSize.B)
            throw FileTooLargeException()

        // Read the file content from the stream
        val byteArrayOutputStream = ByteArrayOutputStream()
        byteArrayOutputStream.use {
            tarArchiver.unarchive(tarArchiveInputStream, byteArrayOutputStream, tarEntry)
        }

        return ActionResource(
            uri = URI("file:$path"),
            content = byteArrayOutputStream.toByteArray(),
            metadata = ActionResourceMetadata(
                uid = tarEntry.longUserId.toInt(),
                gid = tarEntry.longGroupId.toInt(),
                tarEntry.mode,
            ),
        )
    }

    /**
     * Creates a tar archive header to match the metadata of the container file.
     *
     * @param actionResource The container file to create the header for.
     *
     * @return The tar archive header entry.
     */
    private fun createTarEntry(actionResource: ActionResource): TarArchiveEntry {
        require(actionResource.uri.scheme == "file") {
            "Only files can be inserted into a tar archive"
        }

        return TarArchiveEntry(actionResource.uri.schemeSpecificPart).apply {
            size = actionResource.content.size.toLong()
            mode = actionResource.metadata.posixPermissions
            groupId = actionResource.metadata.gid
            userId = actionResource.metadata.uid
        }
    }

    /**
     * Inserts a single file into the tar archive.
     *
     * @param actionResource The file to insert into the tar archive.
     * @param tarArchiveOutputStream The tar archive output stream to insert the file into.
     */
    private fun insertFile(actionResource: ActionResource, tarArchiveOutputStream: TarArchiveOutputStream) {
        val entry = createTarEntry(actionResource)
        ByteArrayInputStream(actionResource.content).use { inputStream ->
            tarArchiver.archive(inputStream, tarArchiveOutputStream, entry)
        }
    }
}
