package nl.rug.digitallab.themis.runtime.container.util.docker.clients

import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.netty.NettyDockerCmdExecFactory

/**
 * This function generates the netty factory used for docker-java's netty transport.
 * This transport should be used *as little as possible*.
 * Please refer to [DockerClientStore] for the proper transport over Apache HTTP5.
 *
 * The reason that this transport is used is that docker-java's attach command is bugged on nearly all transports, expect for Netty.
 * For other transports, the attached stdin stream does not close when the stream is closed, leading to the client not receiving an EOF.
 *
 * As such, this client should only be used to handle the "attach" request.
 *
 * The Netty transport used npipe, which does not work on Windows.
 *
 * @return The factory for generating docker commands over Netty.
 */
fun nettyFactory(): NettyDockerCmdExecFactory {
    val factory = NettyDockerCmdExecFactory()

    val config: DockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder().build()
    factory.init(config)

    return factory
}
