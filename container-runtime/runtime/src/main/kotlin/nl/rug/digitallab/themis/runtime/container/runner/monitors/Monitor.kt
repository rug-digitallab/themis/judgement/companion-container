package nl.rug.digitallab.themis.runtime.container.runner.monitors

import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext

/**
 * A monitor is a single module within the container runner that is responsible for monitoring a specific aspect of the container.
 * They can deal with observing container aspects like memory usage, output logs, wall time, etc.
 *
 * The container runner has a list of monitors it needs to initialise when running a container.
 */
interface Monitor {
    /**
     * Start the monitor and make it perform its monitoring task.
     * This is the entrypoint for the monitor.
     */
    context(ActionContext)
    suspend fun startMonitor()
}
