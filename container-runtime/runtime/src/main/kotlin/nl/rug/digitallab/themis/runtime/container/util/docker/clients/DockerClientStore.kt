package nl.rug.digitallab.themis.runtime.container.util.docker.clients

import com.github.dockerjava.api.DockerClient
import com.github.dockerjava.core.DefaultDockerClientConfig
import com.github.dockerjava.core.DockerClientConfig
import com.github.dockerjava.core.DockerClientImpl
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient
import com.github.dockerjava.transport.DockerHttpClient
import jakarta.enterprise.context.Dependent
import jakarta.enterprise.inject.Produces
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerConfig
import java.time.Duration

/**
 * Service for providing dependency injection for a docker client.
 *
 * This client store uses the Apache HTTP5 transport. It is the main transport that should be used for all docker requests.
 * The only exception is "attach", which must be handled through the Netty transport, as attach is broken on many of the transports.
 */
@Dependent
class DockerClientStore {
    @Inject
    private lateinit var dockerConfig: DockerConfig

    /**
     * Builds a docker client and returns it.
     *
     * @return The docker client.
     */
    @Produces
    fun dockerClient(): DockerClient {
        // Config for the Docker client
        val config: DockerClientConfig = DefaultDockerClientConfig.createDefaultConfigBuilder().build()

        // The actual object used to make HTTP requests to docker
        val httpClient: DockerHttpClient = ApacheDockerHttpClient.Builder()
            .dockerHost(config.dockerHost)
            .sslConfig(config.sslConfig)
            .maxConnections(dockerConfig.maxConnections())
            .connectionTimeout(Duration.ofSeconds(dockerConfig.connectionTimeoutSeconds()))
            .responseTimeout(Duration.ofSeconds(dockerConfig.responseTimeoutSeconds()))
            .build()

        return DockerClientImpl.getInstance(config, httpClient)
    }
}
