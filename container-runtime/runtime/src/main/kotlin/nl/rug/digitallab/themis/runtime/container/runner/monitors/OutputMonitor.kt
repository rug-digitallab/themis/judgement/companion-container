package nl.rug.digitallab.themis.runtime.container.runner.monitors

import com.github.dockerjava.api.exception.ConflictException
import com.github.dockerjava.api.exception.DockerException
import com.github.dockerjava.api.exception.NotFoundException
import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.api.model.StreamType
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.callback.DockerCallback
import kotlin.collections.plus

/**
 * Monitors the output of a container, and kills it if it exceeds the output limit.
 */
@ApplicationScoped
class OutputMonitor : Monitor {
    @Inject
    private lateinit var dockerManager: DockerManager

    /**
     * Set up a callback to monitor stdout and stderr of a container to both log it, and check if it doesn't exceed the limit.
     *
     * This is the main entrypoint for the monitor.
     */
    context(ActionContext)
    override suspend fun startMonitor() {
        val callback = DockerCallback<Frame?>(logger) {
            receiveNewContainerOutput(it)
        }
        dockerManager.logContainer(callback, resourceId)
    }

    /**
     * This function is passed as a higher-order function to the [DockerCallback] to process new output from the container.
     * It is called upon receiving a new message from the container, like a new line of output on stdout or stderr.
     *
     * @param receivedMsg The message received from the container.
     */
    context(ActionContext)
    private fun receiveNewContainerOutput(receivedMsg: Frame?) {
        // Check if the message is valid
        if (receivedMsg == null || receivedMsg.payload == null || receivedMsg.payload.isEmpty()) {
            logger.error("Container $resourceId sent an invalid message to the logger")
            return
        }

        // Check if the output limit is exceeded. Do not add the message to the output if it is, to protect against enormous single payloads
        // This guarantees that the returned stream's size is always less than or equal to the limit.
        if (
            (stdout.size + stderr.size + receivedMsg.payload.size) > executionLimits.output.B  // Check if the limit is exceeded
            && outputLimitExceeded.compareAndSet(false, true)                                  // Only kill the container once
        ) {
            handleOutputLimitExceeded()
        } else if (!outputLimitExceeded.get()) { // Do not keep collecting output if the limit is already exceeded
            processMessage(receivedMsg)
        }
    }

    /**
     * This function is called when the output limit is exceeded. It handles all the necessary steps involved in exceeding the limit.
     * It will kill the container, report the output exceeded status, and log the event.
     */
    context(ActionContext)
    private fun handleOutputLimitExceeded() {
        try {
            // Try to kill the container
            dockerManager.killContainer(resourceId)
        } catch (e: DockerException) {
            when (e) {
                // Container already died. Likely to be a race condition where the container finished just before the monitor.
                is NotFoundException, is ConflictException -> {
                    logger.warn("Failed to kill container $resourceId after exceeding output limit." +
                            "It likely already finished. Still marking as exceeded output.")
                }
                else -> throw e
            }
        }
        reportStatus(ActionStatus.EXCEEDED_STREAM_OUTPUT)
        logger.info("Container $resourceId exceeded its output limit, terminating...")
    }

    /**
     * This function processes the received message from the container and stores it in the correct output stream.
     * At this point, the message has already been validated for any issues, and we can safely process it.
     *
     * @param receivedMsg The message received from the container that needs to be processed.
     */
    context(ActionContext)
    private fun processMessage(receivedMsg: Frame) {
        when(receivedMsg.streamType) {
            StreamType.STDOUT -> {
                stdout += receivedMsg.payload
            }
            StreamType.STDERR -> {
                stderr += receivedMsg.payload
            }
            else -> {
                logger.error("Container $resourceId sent message on unknown channel: \"$receivedMsg\"")
            }
        }
    }
}
