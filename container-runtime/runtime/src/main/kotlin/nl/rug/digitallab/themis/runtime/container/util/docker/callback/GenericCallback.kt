package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import org.jboss.logging.Logger

/**
 * A callback to process the output of generic docker commands.
 * This class should be used when you simply want to log the output produced by a docker command, and do not need to process it further.
 *
 * @param T The type of the response.
 */
class GenericCallback<T> : DockerCallback<T>(
    logger = Logger.getLogger(GenericCallback::class.java),
    messageProcessor = {
        Logger.getLogger(GenericCallback::class.java).info(it.toString())
    }
)
