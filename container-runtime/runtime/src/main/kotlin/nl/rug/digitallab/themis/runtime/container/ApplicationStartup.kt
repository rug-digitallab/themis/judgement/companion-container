package nl.rug.digitallab.themis.runtime.container

import io.quarkus.runtime.StartupEvent
import io.smallrye.config.ConfigMapping
import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.event.Observes
import jakarta.inject.Inject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import nl.rug.digitallab.themis.runtime.container.monitor.services.ProcessService
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import org.jboss.logging.Logger
import kotlin.io.path.Path
import kotlin.io.path.createDirectory
import kotlin.io.path.exists

/**
 * Holds any configuration relevant to the startup process of the container runtime.
 */
@ConfigMapping(prefix = "digital-lab.container-runtime.startup")
interface StartupConfig {
    /**
     * Whether to run verification checks of the environment during startup.
     *
     * @return True if verification checks should be run, false otherwise.
     */
    fun verifyEnvironment(): Boolean
}

/**
 * This class hooks into the application startup process.
 * It is responsible for any task that should be run during startup, such as verification or service initialization.
 *
 * One of the services that is started during the startup process is the [ProcessService].
 * When gVisor containers are started by the application runtime, they talk to this endpoint.
 * The process monitor uses this endpoint to track processes running inside the container started by the container runtime.
 *
 * For the actual main gRPC entrypoint of the container runtime: see [nl.rug.digitallab.themis.runtime.common.services.GrpcService].
 */
@ApplicationScoped
class ApplicationStartup {
    @Inject
    private lateinit var processService: ProcessService

    @Inject
    private lateinit var dockerManager: DockerManager

    @Inject
    private lateinit var config: StartupConfig

    @Inject
    private lateinit var logger: Logger

    /**
     * Verifies basic requirements for the Docker Daemon.
     *
     * This is not a comprehensive check, it only verifies the information that is exposed via the Docker API.
     * For example, it will verify whether the user is using an XFS filesystem, but not whether prjquota is enabled.
     * That second piece of information is not exposed via the Docker API.
     */
    private fun verifyDaemon() {
        if (!config.verifyEnvironment())
            return

        val info = dockerManager.daemonInfo()

        require(info.runtimes.containsKey("runsc")) {
            "gVisor runtime not found in the Docker Daemon.\n" +
            "Please make sure the gVisor runtime is installed and added as 'runsc' in daemon.json."
        }

        val backingFilesystem = info.driverStatuses?.find { it[0] == "Backing Filesystem" }?.get(1)
        require(backingFilesystem == "xfs") {
            "The docker daemon must be backed by an XFS filesystem to use the container runtime."
        }

        require(info.cGroupVersion == "2") {
            "The docker daemon must use cgroups v2 to use the container runtime."
        }

        // The directory in which we will do Sequenced Socket communication with the gVisor container.
        val seqSocketDir = Path("/tmp/themis")
        if (!seqSocketDir.exists()) {
            seqSocketDir.createDirectory()
            logger.warn("IMPORTANT: /tmp/themis does not exist, so it was created automatically.\n" +
                        "This only works if the container runtime is running natively on the host.\n" +
                        "If it is running in a container, you likely forgot to mount /tmp/themis from the host.")
        }
    }

    /**
     * Runs custom functions during the Quarkus startup process.
     * These startup functions verify integrity and start services.
     *
     * @param ev The startup event that triggers this function.
     */
    fun onStart(
        @Observes ev: StartupEvent?,
        coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.IO),
    ) {
        verifyDaemon()

        coroutineScope.launch {
            processService.startService()
        }
    }
}
