package nl.rug.digitallab.themis.runtime.container.runner

import com.github.dockerjava.api.command.InspectContainerResponse
import io.quarkus.arc.All
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.*
import kotlinx.datetime.Instant
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.runtime.common.runner.ActionRunner
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.GlobalMonitorLookup
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionOutput
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionResult
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.container.runner.monitors.*
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import org.jboss.logging.Logger
import java.net.URI
import kotlin.time.Duration

/**
 * A container executed by the container runtime that executes something in the judgement pipeline.
 * The container runs as a docker container.
 */
@ApplicationScoped
class ContainerRunner : ActionRunner {
    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var dockerManager: DockerManager

    @Inject
    private lateinit var fileHandler: ContainerFileHandler

    @All
    private lateinit var monitors: MutableList<Monitor>

    /**
     * Runs a container managed by the container runtime.
     * This will create the container, manage all features implemented by the container runtime, start the container, and clean up afterward.
     *
     * @param configuration The configuration for the container.
     */
    override suspend fun runAction(
        configuration: ActionConfiguration
    ): ActionResult = withCoroutineSpan("RUN container", Dispatchers.IO) {
        // Pull image
        if (!dockerManager.imageExists(configuration.image)) {
            logger.info("Pulling image ${configuration.image}")
            dockerManager.pullImage(configuration.image)
        }

        return@withCoroutineSpan withContainer(configuration) {
            // Insert the input files into the container
            fileHandler.insertFiles(configuration.inputs.filter { it.uri.scheme == "file" })

            // Attach stdin to the container.
            // Only allows for one file to be attached, which is the first stream file found.
            dockerManager.attachStdIn(
                configuration.inputs.firstOrNull { it.uri == URI("stream:stdin") },
                resourceId
            )

            // Start the container
            dockerManager.startContainer(resourceId)

            // Execute all the monitoring modules that are responsible for various parts of the observability of the container.
            val jobs = monitors.map { monitor ->
                async {
                    monitor.startMonitor()
                }
            }
           jobs.awaitAll()

            // Update all the information about the container found in docker inspect.
            reportContainerInspection(configuration.executionLimits.wallTime)

            // Retrieve the output files from the container
            val outputFiles = fileHandler.retrieveFiles(configuration.outputRequests)

            // Write down the amount of KB written to stdout and stderr
            executionMetrics.output = (stderr.size + stdout.size).B

            logger.info("Container $resourceId finished with status $actionExecStatus")

            retrieveProcessMonitorResults(configuration)

            return@withContainer ActionResult(
                status = actionExecStatus,
                exitCode = exitCode,
                outputs = listOf(
                    ActionOutput.fromOutStream("stdout", stdout),
                    ActionOutput.fromOutStream("stderr", stderr),
                ) + outputFiles,
                executionMetrics = executionMetrics
            )
        }
    }

    /**
     * This functions communicates with the process monitor, and uses it to adjust the status of the action.
     * This includes:
     * - Checking whether the container exceeded the user process limit sometime during its execution.
     */
    context(ActionContext)
    suspend fun retrieveProcessMonitorResults(actionConfiguration: ActionConfiguration) {
        val monitor = GlobalMonitorLookup.getMonitor(resourceId)
            ?: throw IllegalStateException("No process monitor found for container $resourceId.\n" +
                    "This may be because '/etc/docker/daemon.json' or '/etc/themis/process_monitor.json' is not properly set up.")

        // Give the monitor 5 seconds to finish
        // It should normally already be done the moment the container finishes, but this is a safety measure
        monitor.awaitFinish()

        val maxUserProcesses = monitor.maxUserProcessCount
        if (maxUserProcesses > actionConfiguration.executionLimits.userProcesses) {
            reportStatus(ActionStatus.EXCEEDED_PROCESSES)
            logger.info("Container $resourceId exceeded the user process limit. Max processes: $maxUserProcesses, limit: ${actionConfiguration.executionLimits.userProcesses}")
        }

        // Set used user processes
        executionMetrics.maxProcesses = maxUserProcesses
        executionMetrics.createdProcesses = monitor.totalCreatedProcesses
    }

    /**
     * Injects a container context into the provided block.
     *
     * This container context represents a real container that is managed by the container runtime.
     * This function will take care of creating and cleaning up the container.
     * One can assume that a container is available within the provided block, and that it will be cleaned up afterward.
     *
     * @param actionConfiguration The configuration for the container.
     * @param block The block to execute with the container context.
     */
    suspend fun withContainer (
        actionConfiguration: ActionConfiguration,
        block: suspend context(ActionContext) () -> ActionResult,
    ): ActionResult {
        // Create the container
        val containerContext = dockerManager.createContainer(actionConfiguration)

        try {
            return block(containerContext)
        } finally {
            // Clean up the container
            dockerManager.deleteContainer(containerContext.resourceId)
        }
    }

    /**
     * This functions parses the response of `docker inspect` to update the container's status, exit code, and resource usage.
     *
     * @param wallTimeLimit The amount of the time the container is allowed to run.
     */
    context(ActionContext)
    fun reportContainerInspection(wallTimeLimit: Duration) {
        val containerInfo = dockerManager.inspectContainer(resourceId)

        val exitCode = containerInfo.state.exitCodeLong
            ?: throw IllegalStateException("Container $resourceId has no exit code")
        reportExitCode(exitCode.toInt())

        if (containerInfo.state.oomKilled == true) {
            reportStatus(ActionStatus.EXCEEDED_MEMORY)
        } else if (exitCode != 0L) {
            reportStatus(ActionStatus.CRASHED)
        }

        reportContainerDuration(containerInfo.state, wallTimeLimit)

        // Set the amount of disk space used by the container
        // May overflow as sizeRw is an int. Awaiting fix to docker-java to be merged.
        executionMetrics.disk = containerInfo.sizeRw.B
    }

    /**
     * Parse the container's start and finish time to calculate the duration of the container.
     * This information is used to check if the container exceeded its wall time limit, and to record the amount of time the container took to complete.
     *
     * @param state The state of the container.
     * @param wallTimeLimit The amount of the time the container is allowed to run.
     *
     * @throws IllegalStateException If the container has no start or finish time.
     */
    context(ActionContext)
    private fun reportContainerDuration(state: InspectContainerResponse.ContainerState, wallTimeLimit: Duration) {
        check (state.startedAt != null && state.finishedAt != null) {
            "Container $resourceId does not define both a start and finish time."
        }

        val startedAt = Instant.parse(state.startedAt!!)
        val finishedAt = Instant.parse(state.finishedAt!!)

        val duration = finishedAt - startedAt

        // Record the results from docker's timing
        if (duration > wallTimeLimit) {
            reportStatus(ActionStatus.EXCEEDED_WALL_TIME)
        }

        executionMetrics.wallTime = duration
    }
}
