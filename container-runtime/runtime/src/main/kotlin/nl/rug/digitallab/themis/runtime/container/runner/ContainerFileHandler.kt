package nl.rug.digitallab.themis.runtime.container.runner

import com.github.dockerjava.api.exception.NotFoundException
import io.opentelemetry.instrumentation.annotations.WithSpan
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.runtime.container.exceptions.FileTooLargeException
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionOutputRequest
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionOutput
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.TarHelper
import java.io.PipedInputStream
import java.io.PipedOutputStream

/**
 * This class is responsible for inserting files into and extracting files from the container.
 */
@ApplicationScoped
class ContainerFileHandler {
    @Inject
    private lateinit var dockerManager: DockerManager

    @Inject
    private lateinit var tarHelper: TarHelper

    /**
     * Insert a list of files into a container.
     *
     * @param files The files to insert into the container.
     */
    context(ActionContext)
    suspend fun insertFiles(files: List<ActionResource>) = withCoroutineSpan("INSERT files", Dispatchers.IO) {
        val inputStream = PipedInputStream()

        inputStream.use { stream ->
            PipedOutputStream(stream).use {outputStream ->
                launch { tarHelper.insertFilesIntoStreamAsTar(files, outputStream) }
                val copyJob = launch { dockerManager.copyArchiveIntoContainer(stream, resourceId) }

                // Wait for the copy into the container to finish
                // so the input stream isn't closed before the copy is done
                copyJob.join()
            }
        }
    }

    /**
     * Retrieves a single [ActionResource] from a container.
     *
     * @param requestedFile The request describing the file to retrieve from the container.
     *
     * @return The file retrieved from the container.
     */
    context(ActionContext)
    fun retrieveFile(requestedFile: ActionOutputRequest): ActionResource {
        require(requestedFile.uri.scheme == "file") { "Only file URIs are supported" }

        // Retrieve the file from the container
        val tarStream = dockerManager.copyArchiveFromContainer(requestedFile.uri.schemeSpecificPart, resourceId)

        tarStream.use { stream ->
            // Extract the file from the tar archive stream. Note that the tar stream will only contain one file.
            return tarHelper.extractFileFromTarStream(
                stream,
                requestedFile.uri.schemeSpecificPart,
                requestedFile.maxSize,
            )
        }
    }

    /**
     * Retrieve a list of files from a container.
     *
     * @param requestedFiles The files to retrieve from the container.
     *
     * @return A class representing the function result, containing:
     * 1. The list of files retrieved from the container.
     * 2. A boolean indicating whether one of the files was too large to retrieve.
     */
    context(ActionContext)
    @WithSpan("RETRIEVE files")
    fun retrieveFiles(requestedFiles: List<ActionOutputRequest>): List<ActionOutput> {
        val actionOutputs = mutableListOf<ActionOutput>()

        for (requestedFile in requestedFiles) {
            try {
                actionOutputs.add(
                    ActionOutput(
                    resource = retrieveFile(requestedFile),
                    isPresent = true,
                    sizeExceeded = false,
                )
                )
            } catch (_: NotFoundException) {
                // If the file is not present in the container, add a file marked as non-present
                actionOutputs.add(ActionOutput.nonPresent(requestedFile.uri))
            } catch (_: FileTooLargeException) {
                // If the file is too large, add a file marked as too large
                actionOutputs.add(ActionOutput.tooLarge(requestedFile.uri))
                reportStatus(ActionStatus.EXCEEDED_OUTPUT_FILE)
            }
        }

        return actionOutputs
    }
}
