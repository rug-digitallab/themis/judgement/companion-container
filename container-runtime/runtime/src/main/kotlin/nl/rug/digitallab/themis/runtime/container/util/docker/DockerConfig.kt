package nl.rug.digitallab.themis.runtime.container.util.docker

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping

/**
 * The configuration for the docker client.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.docker")
interface DockerConfig {
    fun maxConnections(): Int
    fun connectionTimeoutSeconds(): Long
    fun responseTimeoutSeconds(): Long
}
