package nl.rug.digitallab.themis.runtime.container.runner.monitors

import com.github.dockerjava.api.exception.ConflictException
import com.github.dockerjava.api.exception.DockerException
import com.github.dockerjava.api.exception.NotFoundException
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.selects.select
import kotlinx.coroutines.withContext
import nl.rug.digitallab.common.quarkus.opentelemetry.withCoroutineSpan
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager

/**
 * The [WallTimeMonitor] is responsible for keeping track of how long the container has been running.
 *
 * It has three responsibilities:
 * 1. Monitor the wall time of the container and kill it if it exceeds the limit.
 * 2. Mark the container as succeeded if it finished on time, or as exceeded the wall time if it was killed.
 * 3. Report the final time taken by the container.
 */
@ApplicationScoped
class WallTimeMonitor : Monitor {
    @Inject
    private lateinit var dockerManager: DockerManager

    /**
     * Set up a coroutine to monitor the wall time of a container and kill it if it exceeds the limit.
     * This function will block until the container is done.
     * It will also mark the container as succeeded if it finished on time, or as exceeded the wall time if it was killed.
     */
    context(ActionContext)
    override suspend fun startMonitor() = withCoroutineSpan("AWAIT container") {
        withContext(Dispatchers.IO) {
            val deferredStopped = async { containerStopped() }
            val deferredTime = async { timeExceeded() }

            // Await for any of the two coroutines to finish.
            // So resume once either the container has stopped, or the wall time limit has been exceeded.
            val status = select {
                deferredStopped.onAwait { it }
                deferredTime.onAwait { it }
            }

            // First EXCEEDED_WALL_TIME check. If the container got killed for taking too long, it will be marked as EXCEEDED_WALL_TIME.
            // Not being marked as such does not mean the container finished on time. The time the container is given until it is killed is very generous.
            // So we will need to also check how long docker said the container was running for to perform a more strict check.
            reportStatus(status)

            deferredTime.cancel()
            deferredStopped.cancel()
        }
    }

    /**
     * Wait for the wall time limit to be exceeded and kill the container if it is.
     * If the container is killed, mark it as having exceeded the wall time.
     */
    context(ActionContext)
    private suspend fun timeExceeded(): ActionStatus {
        delay(executionLimits.wallTime)

        return try {
            logger.info("Killing container $resourceId")
            dockerManager.killContainer(resourceId)

            ActionStatus.EXCEEDED_WALL_TIME
        } catch (e: DockerException) {
            when (e) {
                // Container already died. Likely to be a race condition where the container finished just before the monitor.
                // Let's give the student the benefit of the doubt
                is NotFoundException, is ConflictException -> {
                    logger.info(
                        "Container $resourceId already died, assuming it finished just before/after the wall time limit was exceeded." +
                                "Marking container as not having exceeded the time."
                    )

                    ActionStatus.SUCCEEDED
                }
                else -> throw e
            }
        }
    }

    /**
     * Wait for the container to stop.
     * If the container stops, mark it as succeeded.
     *
     * @return The status of the container.
     */
    context(ActionContext)
    private suspend fun containerStopped(): ActionStatus = withContext(Dispatchers.IO) {
        dockerManager.waitContainer(resourceId)

        return@withContext ActionStatus.SUCCEEDED
    }
}
