package nl.rug.digitallab.themis.runtime.container.runner.monitors

import com.github.dockerjava.api.exception.NotFoundException
import com.github.dockerjava.api.model.Frame
import com.github.dockerjava.api.model.StreamType
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.ContainerRunner
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.callback.DockerCallback
import nl.rug.digitallab.themis.runtime.test.util.getBasicContainerLimits
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.kotlin.*
import kotlin.time.Duration.Companion.seconds

/**
 * Tests for [ContainerRunner]'s output monitoring.
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class OutputMonitoringTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @InjectMock
    lateinit var mockedManager: DockerManager

    @Inject
    lateinit var outputMonitor: OutputMonitor

    /**
     * Sets up the mocked docker manager.
     */
    @BeforeEach
    fun setup() {
        // Mock the docker kill command
        doNothing().whenever(mockedManager).killContainer(any())

        // Mock the docker log command
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<DockerCallback<Frame?>>(0)
            logger.onNext(Frame(StreamType.STDOUT, "This message is 24 bytes".toByteArray()))
            logger.onNext(Frame(StreamType.STDERR, "7 bytes".toByteArray()))
            return@then logger
        }
    }

    @Test
    fun `The output monitor properly collects the stdout of the container`(): Unit = runBlocking {
        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            outputMonitor.startMonitor()

            assertArrayEquals("This message is 24 bytes".toByteArray(), stdout)
        }
    }

    @Test
    fun `The output monitor properly collects the stderr of the container`(): Unit = runBlocking {
        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            outputMonitor.startMonitor()

            assertArrayEquals("7 bytes".toByteArray(), stderr)
        }
    }

    @Test
    fun `The output monitor continues collecting output even after the container has been killed`(): Unit = runBlocking {
        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            outputMonitor.startMonitor()

            assertArrayEquals("7 bytes".toByteArray(), stderr)
        }
    }

    @Test
    fun `Docker kill is called when the container exceeds its output limit`(): Unit = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 1.B,
                limitsConfig = limitsConfig,
            )
        )
        ) {
            outputMonitor.startMonitor()
        }

        Mockito.verify(mockedManager).killContainer(any())
    }

    @Test
    fun `The output monitor stops collecting output after hitting the limit`(): Unit = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 24.B,
                limitsConfig = limitsConfig,
            )
        )
        ) {
            outputMonitor.startMonitor()

            assertArrayEquals("This message is 24 bytes".toByteArray(), stdout)
            assertArrayEquals(byteArrayOf(), stderr)
        }
    }

    @Test
    fun `If the next message exceeds the output limit, the message is dropped`(): Unit = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 20.B,
                limitsConfig = limitsConfig,
            )
        )
        ) {
            outputMonitor.startMonitor()

            assertArrayEquals(byteArrayOf(), stdout)
            assertArrayEquals(byteArrayOf(), stderr)
        }
    }

    @Test
    fun `The output monitor does not fail if a message is sent on an unknown channel`(): Unit = runBlocking {
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<DockerCallback<Frame?>>(0)
            logger.onNext(Frame(StreamType.STDIN, "The monitor should not receive output on STDIN, but should not crash".toByteArray()))
            return@then logger
        }

        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            assertDoesNotThrow {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor does not fail if a message is sent with an empty payload`(): Unit = runBlocking {
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<DockerCallback<Frame?>>(0)
            logger.onNext(Frame(StreamType.STDOUT, byteArrayOf()))
            return@then logger
        }

        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            assertDoesNotThrow {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor does not fail if the message is null`(): Unit = runBlocking {
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<DockerCallback<Frame?>>(0)
            logger.onNext(null)
            return@then logger
        }

        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            assertDoesNotThrow {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor does not fail if the message payload is null`(): Unit = runBlocking {
        whenever(mockedManager.logContainer(any(), any())).then {
            val logger = it.getArgument<DockerCallback<Frame?>>(0)
            logger.onNext(Frame(StreamType.STDOUT, null))
            return@then logger
        }

        with(ActionContext("", Logger.getLogger("testLogger"), getBasicContainerLimits(limitsConfig))) {
            assertDoesNotThrow {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor does not fail when it tries to kill a container that already died`(): Unit = runBlocking {
        doThrow(NotFoundException("")).whenever(mockedManager).killContainer(any())

        with(
            ActionContext("", Logger.getLogger("testLogger"),
                ActionExecutionLimits(
                    wallTime = 1.seconds,
                    cpuCores = 1.0f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 50.MB,
                    disk = 50.MB,
                    output = 0.B,
                    limitsConfig = limitsConfig,
                )
            )
        ) {
            assertDoesNotThrow {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor does fail if it receives an unexpected exception`(): Unit = runBlocking {
        doThrow(NullPointerException()).whenever(mockedManager).killContainer(any())

        with(
            ActionContext("", Logger.getLogger("testLogger"),
                ActionExecutionLimits(
                    wallTime = 1.seconds,
                    cpuCores = 1.0f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 50.MB,
                    disk = 50.MB,
                    output = 0.B,
                    limitsConfig = limitsConfig,
                )
            )
        ) {
            assertThrows<NullPointerException> {
                outputMonitor.startMonitor()
            }
        }
    }

    @Test
    fun `The output monitor state is not persisted across different contexts`(): Unit = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
                ActionExecutionLimits(
                    wallTime = 1.seconds,
                    cpuCores = 1.0f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 50.MB,
                    disk = 50.MB,
                    output = 24.B,
                    limitsConfig = limitsConfig,
                )
            )
        ) {
            outputMonitor.startMonitor()

            assertArrayEquals("This message is 24 bytes".toByteArray(), stdout)
            assertArrayEquals(byteArrayOf(), stderr)
        }

        with(
            ActionContext("", Logger.getLogger("testLogger"),
                ActionExecutionLimits(
                    wallTime = 1.seconds,
                    cpuCores = 1.0f,
                    actionProcesses = 50,
                    userProcesses = 5,
                    memory = 50.MB,
                    disk = 50.MB,
                    output = 50.B,
                    limitsConfig = limitsConfig,
                )
            )
        ) {
            outputMonitor.startMonitor()

            assertArrayEquals("This message is 24 bytes".toByteArray(), stdout)
            assertArrayEquals("7 bytes".toByteArray(), stderr)
        }
    }
}
