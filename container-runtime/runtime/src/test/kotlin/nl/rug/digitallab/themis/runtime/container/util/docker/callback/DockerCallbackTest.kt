package nl.rug.digitallab.themis.runtime.container.util.docker.callback

import io.quarkus.test.junit.QuarkusTest
import org.jboss.logging.Logger
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.mock

@QuarkusTest
class DockerCallbackTest {
    @Test
    fun `An error is properly logged by the docker callback`() {
        val logger: Logger = mock()

        val callback = DockerCallback<String>(logger) {
            throw Exception("This callback should not be handling normal messages")
        }

        callback.onError(Exception("This is an error"))

        Mockito.verify(logger).error("Callback produced error: \"This is an error\"")
    }
}
