package nl.rug.digitallab.themis.runtime.container.runner.monitors

import com.github.dockerjava.api.exception.NotFoundException
import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.container.runner.ContainerRunner
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import org.jboss.logging.Logger
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.*
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

/**
 * Tests for [ContainerRunner]'s wall time monitor.
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class WallTimeMonitorTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @InjectMock
    private lateinit var mockedManager: DockerManager

    @Inject
    private lateinit var monitor: WallTimeMonitor

    /**
     * Set up the mocked docker manager.
     */
    @BeforeAll
    fun setup() {
        // Mock the docker wait command as a container which takes 2s to finish
        whenever(mockedManager.waitContainer(any())).then {
            Thread.sleep(2000)
            0 // Exit code
        }

        // Mock the docker kill command
        doNothing().`when`(mockedManager).killContainer(any())

        QuarkusMock.installMockForType(mockedManager, DockerManager::class.java)
    }

    @Test
    fun `A container is flagged as exceeded wall time if it finishes too late`() = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        )
        ) {
            monitor.startMonitor()

            assertEquals(ActionStatus.EXCEEDED_WALL_TIME, actionExecStatus)
        }
    }

    @Test
    fun `A container is not flagged as exceeded wall time if it finishes on time`() = runBlocking {
        with(
            ActionContext("", Logger.getLogger("testLogger"),
            ActionExecutionLimits(
                wallTime = 3.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        )
        ) {
            monitor.startMonitor()

            assertEquals(ActionStatus.SUCCEEDED, actionExecStatus)
        }
    }

    @Test
    fun `A container that finishes right before the wall time limit is not flagged as exceeded wall time`() = runBlocking {
        whenever(mockedManager.killContainer(any())).thenThrow(NotFoundException::class.java)

        with(ActionContext("", Logger.getLogger("testLogger"), basicWallTimeTestLimits())) {
            monitor.startMonitor()

            assertEquals(ActionStatus.SUCCEEDED, actionExecStatus)
        }
    }

    @Test
    fun `A container that failed for another reason but is given the benefit of the doubt on walltime is not marked as succeeded`() = runBlocking {
        whenever(mockedManager.killContainer(any())).thenThrow(NotFoundException::class.java)

        with(ActionContext("", Logger.getLogger("testLogger"), basicWallTimeTestLimits())) {
            reportStatus(ActionStatus.EXCEEDED_STREAM_OUTPUT)

            monitor.startMonitor()

            assertEquals(ActionStatus.EXCEEDED_STREAM_OUTPUT, actionExecStatus)
        }
    }

    private fun basicWallTimeTestLimits() = ActionExecutionLimits(
        wallTime = 100.milliseconds,
        cpuCores = 1.0f,
        actionProcesses = 50,
        userProcesses = 5,
        memory = 50.MB,
        disk = 50.MB,
        output = 5.MB,
        limitsConfig = limitsConfig,
    )
}
