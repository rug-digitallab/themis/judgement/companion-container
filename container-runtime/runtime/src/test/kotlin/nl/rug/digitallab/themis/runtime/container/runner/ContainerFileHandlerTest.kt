package nl.rug.digitallab.themis.runtime.container.runner

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourceMetadata
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionOutputRequest
import nl.rug.digitallab.themis.runtime.container.util.docker.DockerManager
import nl.rug.digitallab.themis.runtime.container.util.docker.TarHelper
import nl.rug.digitallab.themis.runtime.test.util.getBasicContainerLimits
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.io.PipedInputStream
import java.net.URI

/**
 * Tests for [ContainerFileHandler].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ContainerFileHandlerTest {
    @InjectMock
    private lateinit var mockedManager: DockerManager

    @Inject
    private lateinit var fileHandler: ContainerFileHandler

    @Inject
    private lateinit var limits: ResourceLimitsConfig

    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var tarArchiver: TarArchiver

    @Test
    fun `A file can be inserted into a container`() {
        runBlocking {
            // Receives a request to upload a tar archive, and checks whether it contains the "Test message" message passed in the unit test.
            // Used for the insertFilesTest
            whenever(mockedManager.copyArchiveIntoContainer(any(), any())).then {
                val stream = it.getArgument<PipedInputStream>(0)
                tarArchiver.createArchiveInputStream(stream).use { tarStream ->
                    val header = tarStream.nextEntry
                    val content = stream.readNBytes(header.size.toInt())
                    assertArrayEquals(content, "Test message".toByteArray())

                    stream.readBytes()
                }
            }
        }

        with(ActionContext("", logger, getBasicContainerLimits(limits))) {
            runBlocking {
                // Will fail if the mocked docker manager does not receive a tar archive containing the "Test message" message
                fileHandler.insertFiles(
                    listOf(
                        ActionResource(
                            URI("file:test.txt"),
                            "Test message".toByteArray(),
                            ActionResourceMetadata(
                                0,
                                0,
                                0,
                            )
                        )
                    )
                )
            }
        }
    }

    @Test
    fun `A file can be extracted from a container`() {
        val mockedTarHelper: TarHelper = mock()

        // Set up the mock
        runBlocking {
            whenever(mockedManager.copyArchiveFromContainer(any(), any())).then {
                val tarStream: TarArchiveInputStream = mock()

                whenever(tarStream.readNBytes(12)).thenReturn("Test message".toByteArray())

                return@then tarStream
            }

            whenever(mockedTarHelper.extractFileFromTarStream(any(), any(), any())).then {
                val path = it.getArgument<String>(1)
                val maxSize = it.getArgument<Long>(2)

                assertEquals(1.MB, maxSize.B)

                val file = ActionResource(
                    URI("file:$path"),
                    "Test message".toByteArray(),
                    ActionResourceMetadata(
                        0,
                        0,
                        0,
                    )
                )

                return@then file
            }

            QuarkusMock.installMockForType(mockedTarHelper, TarHelper::class.java)
        }

        val request = ActionOutputRequest(URI("file:test.txt"), 1.MB)

        with(ActionContext("", logger, getBasicContainerLimits(limits))) {
            val file =  fileHandler.retrieveFile(request)

            assertEquals(URI("file:test.txt"), file.uri)
            assertArrayEquals("Test message".toByteArray(), file.content)
        }
    }
}
