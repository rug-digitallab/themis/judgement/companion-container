package nl.rug.digitallab.themis.runtime.container.runner.monitors

import io.quarkus.test.InjectMock
import io.quarkus.test.Mock
import io.quarkus.test.junit.QuarkusTest
import io.smallrye.config.SmallRyeConfig
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.common.kotlin.helpers.ephemeral.withEphemeralDirectory
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.GB
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.contexts.ActionContext
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.kotlin.whenever
import java.io.File
import java.util.*
import kotlin.time.Duration.Companion.seconds

@QuarkusTest
class MemoryMonitorTest {
    @Inject
    private lateinit var memoryMonitor: MemoryMonitor

    @Inject
    lateinit var smallRyeConfig: SmallRyeConfig

    @ApplicationScoped
    @Mock
    fun featuresConfig(): CGroupConfig {
        return smallRyeConfig.getConfigMapping(CGroupConfig::class.java)
    }

    @InjectMock
    lateinit var cgroupConfig: CGroupConfig

    @Inject
    lateinit var logger: Logger

    @Inject
    lateinit var resourceLimitsConfig: ResourceLimitsConfig

    @Test
    fun `The monitor correctly reads the peak memory value of the file`() = runBlocking {
        withEphemeralDirectory(prefix = "cgroup-") { cgroupDirectory ->
            val tempDir = cgroupDirectory.toFile().path
            val containerId = UUID.randomUUID().toString()
            val directory = File("$tempDir/$containerId")
            directory.mkdirs()

            // Create a file with a peak memory value
            val file = File(directory.path + "/memory.peak")
            file.writeText("1234567890")

            Thread {
                Thread.sleep(200)
                file.delete()
            }.start()

            whenever(cgroupConfig.baseDirectory()).thenReturn(tempDir)

            val context = ActionContext(
                resourceId = containerId,
                executionLimits = ActionExecutionLimits(
                    wallTime = 1.seconds,
                    cpuCores = 1f,
                    actionProcesses = 50,
                    userProcesses = 50,
                    memory = 1.GB,
                    disk = 1.GB,
                    output = 1.GB,
                    limitsConfig = resourceLimitsConfig,
                ),
                logger = logger,
            )

            with(context) {
                memoryMonitor.startMonitor()
            }

            assertEquals(1234567890.B, context.executionMetrics.memory)
        }
    }
}
