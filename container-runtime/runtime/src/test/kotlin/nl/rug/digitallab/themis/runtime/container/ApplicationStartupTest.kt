package nl.rug.digitallab.themis.runtime.container

import io.quarkus.test.junit.QuarkusMock
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.advanceUntilIdle
import nl.rug.digitallab.themis.runtime.container.monitor.services.ProcessService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.kotlin.verify

@QuarkusTest
class ApplicationStartupTest {
    @Inject
    private lateinit var application: ApplicationStartup

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `The process monitor is started on application startup`() {
        val mockedMonitor = mock(ProcessService::class.java)

        QuarkusMock.installMockForType(mockedMonitor, ProcessService::class.java)

        // Use a custom coroutine scope so we can control the async behaviour in the test
        // This prevents a flaky test, as otherwise there would be a race condition between
        // `processService.startService()` and `verify(mockedMonitor).startService()`.
        val coroutineScope = TestScope()

        application.onStart(
            null,
            coroutineScope = coroutineScope,
        )

        // Wait until the process monitor has been started
        coroutineScope.advanceUntilIdle()

        runBlocking { verify(mockedMonitor).startService() }
    }

    @Test
    fun `Application startup does not infinitely block`() {
        application.onStart(null)

        // The test will run infinitely long if the application startup is blocking
    }
}
