package nl.rug.digitallab.themis.runtime.container.util.docker

import com.kamelia.sprinkler.util.closeableScope
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.B
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.common.quarkus.archiver.TarArchiver
import nl.rug.digitallab.themis.runtime.container.exceptions.FileTooLargeException
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourceMetadata
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.PipedInputStream
import java.io.PipedOutputStream
import java.net.URI

/**
 * Tests for [TarHelper].
 */
@QuarkusTest
class TarHelperTest {
    @Inject
    lateinit var tarHelper: TarHelper

    @Inject
    lateinit var tarArchiver: TarArchiver

    @Test
    fun `The TarHelper can insert files into a stream as a TAR`() {
        val insertableFiles = listOf(
            ActionResource(
                URI("file:test1.txt"),
                "Test message 1".toByteArray(),
                ActionResourceMetadata(0, 0, 0)
            ),
            ActionResource(
                URI("file:test2.txt"),
                "Test message 2".toByteArray(),
                ActionResourceMetadata(0, 0, 0)
            ),
            ActionResource(
                URI("file:test3.txt"),
                "Test message 3".toByteArray(),
                ActionResourceMetadata(0, 0, 0)
            )
        )

        val inputStream = PipedInputStream()
        PipedOutputStream(inputStream).use { outputStream ->
            Thread { tarHelper.insertFilesIntoStreamAsTar(insertableFiles, outputStream) }.start()
            testStreamOutput(inputStream, insertableFiles)
        }
    }

    @Test
    fun `The TarHelper can extract a file from a tar stream and parse it`() = withDummyInputOutputStream { dummyInputStream, dummyOutputStream ->
        val tarArchiveOutputStream = tarArchiver.createArchiveOutputStream(dummyOutputStream)
        val byteArrayInputStream = ByteArrayInputStream("Test message".toByteArray())

        byteArrayInputStream.use {
            val entry = TarArchiveEntry("test.txt").apply {
                size = 12
            }

            tarArchiver.archive(byteArrayInputStream, tarArchiveOutputStream, entry)

            val file = tarHelper.extractFileFromTarStream(dummyInputStream, "test.txt", 1.MB)

            assertEquals(URI("file:test.txt"), file.uri)
            assertArrayEquals("Test message".toByteArray(), file.content)
        }
    }

    @Test
    fun `The TarHelper can detect when a file is too large`() = withDummyInputOutputStream { dummyInputStream, dummyOutputStream ->
        val tarArchiveOutputStream = tarArchiver.createArchiveOutputStream(dummyOutputStream)
        val byteArrayInputStream = ByteArrayInputStream("Test message".toByteArray())

        byteArrayInputStream.use {
            val entry = TarArchiveEntry("test.txt").apply {
                size = 12
            }

            tarArchiver.archive(byteArrayInputStream, tarArchiveOutputStream, entry)

            assertThrows<FileTooLargeException>{ tarHelper.extractFileFromTarStream(dummyInputStream, "test.txt", 10.B) }
        }
    }


    @Test
    fun `The TarHelper detects when a header is null`() = withDummyInputOutputStream { dummyInputStream, dummyOutputStream ->
        val tarArchiveOutputStream = tarArchiver.createArchiveOutputStream(dummyOutputStream)
        tarArchiveOutputStream.close() // Empty tar stream

        assertThrows<IllegalStateException>{ tarHelper.extractFileFromTarStream(dummyInputStream, "test.txt", 1.MB) }
    }

    /**
     * Test the output of the [TarHelper.insertFilesIntoStreamAsTar]'s stream, testing if it matches the input.
     *
     * @param inputStream The input stream, containing the tar archive, to test.
     * @param insertableFiles The list of files that should be in the tar archive.
     */
    fun testStreamOutput(inputStream: InputStream, insertableFiles: List<ActionResource>) =
        inputStream.use { inputStream ->
            val tarStream = tarArchiver.createArchiveInputStream(inputStream)

            var nextEntry = tarStream.nextEntry
            var idx = 0
            while(nextEntry != null) {
                val file = insertableFiles[idx]
                assertNotNull(file)
                assertEquals(file.uri.schemeSpecificPart, nextEntry.name)
                assertEquals(file.content.size.toLong(), nextEntry.size)
                assertEquals(file.metadata.posixPermissions, nextEntry.mode)
                assertEquals(file.metadata.gid.toLong(), nextEntry.longGroupId)
                assertEquals(file.metadata.uid.toLong(), nextEntry.longUserId)

                ++idx
                nextEntry = tarStream.nextEntry
            }
        }

    /**
     * Create a dummy input and output stream for testing, the streams are connected. The streams are closed after the
     * block is executed.
     *
     * @param block The block to execute with the dummy input and output stream.
     *
     * @return The result of the block.
     */
    fun withDummyInputOutputStream(block: (PipedInputStream, PipedOutputStream) -> Unit) {
        val dummyOutputStream = PipedOutputStream()
        val dummyInputStream = PipedInputStream()
        dummyOutputStream.connect(dummyInputStream)

        closeableScope(dummyInputStream, dummyOutputStream) {
            block(dummyInputStream, dummyOutputStream)
        }
    }
}
