# Summary
This folder contains deployment configurations used for setting up the container runtime's runtime

# Files
- monitor-syscalls.json: Configuration for the Process monitor for detecting zombies/orphans.
- runsc-docker.json: Docker daemon configuration to enable support for gVisor.