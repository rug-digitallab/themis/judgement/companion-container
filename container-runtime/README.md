# Container Runtime
The container runtime is responsible for running untrusted containerized workloads. In a generalised context, this service receives gRPC function calls to run containerized jobs. The container runtime will run these containerized jobs, and return the results along with execution metrics as the function's result. In a Themis-specific context, this service will run the actual student code to test the correctness and efficiency of the student code.

This service places an emphasis on untrusted workloads. Requests made to this service may include malicious programs, which should be safely handled. To safely process this, the container runtime makes use of gVisor for secure containerisation. Furthermore, it places limits on execution time and available resources.

## Requirements
Even though the container runtime runs in a container, it is designed to use the host's docker daemon. As such, it has a few requirements on the host system to be able to run the service, even if the service is containerized.

### Host requirements
The host machine must have the following configurations:
- Linux as the operating system (Especially Windows will not work)
- XFS filesystem with prjquota enabled mounted at `/var/lib/docker` (e.g. `mount -o prjquota -t xfs xfs.bin /var/lib/docker`).
- Docker and gVisor installed. Dockerd should be started with gVisor added (`dockerd --add-runtime runsc=/usr/local/bin/runsc &`).
- `/config/runsc-docker.json` in this repository should be placed at `/etc/docker/daemon.json`.
- `/config/monitor-syscalls.json` should be available at `/etc/themis/process_monitor.json`.
- `/tmp/themis` should have at least permissions of `770`.
- Docker should make use of cgroups v2.

### Requirements for running the container
Running the container runtime itself in a container also has some requirements. These are:
- Container should be privileged (`--privileged`)
- User set as `root`
- External communication:
  - Port 9000 should be exposed for gRPC communication to make requests
  - `/var/run/docker.sock:/var/run/docker.sock` volume for the container runtime to talk to the host's docker daemon.
  - `/tmp/themis:/tmp/themis` volume for gVisor to send container metrics to the container runtime.
  - `/sys/fs/cgroup/docker:/docker/cgroups` for access to cgroup metrics.

## Supported features from the communication spec
This section describes the features of the communication spec that are supported by this runtime.
Some of these features may still need to be implemented, some others may not be possible to implement due to the nature of the runtime.

### Request
- [x] Resource limits
  - [x] Walltime
  - [x] CPU cores (fractional)
  - [x] Memory
  - [x] Disk space
  - [x] Action processes
  - [x] User processes
  - [x] Output size
- [x] Environment
- [x] Startup command
- [x] Environment variables
- [x] Input files
- [x] Requested output files
- [x] Stdin
- [x] Features
  - [ ] Zombie/Orphan monitor
  - [x] Disable/Enable public network access

### Response
- [x] Exit code
- [ ] Status
  - [x] SUCCEEDED
  - [x] CRASHED
  - [x] EXCEEDED_WALL_TIME
  - [x] EXCEEDED_MEMORY
  - [x] EXCEEDED_STREAM_OUTPUT
  - [X] EXCEEDED_OUTPUT_FILE
  - [x] EXCEEDED_PROCESSES (Only for user processes exceeded)
  - [ ] PRODUCED_ORPHANS
  - [ ] PRODUCED_ZOMBIES
  - [ ] PRODUCED_ZOMBIES_AND_ORPHANS
- [x] Stdout stream
- [x] Stderr stream
- [x] Output files
- [ ] Resource usage
  - [x] Walltime
  - [x] Memory
  - [x] Disk space used
  - [x] Output size
  - [x] Created processes
  - [x] Max processes
  - [ ] Orphaned processes
  - [ ] Zombie processes
  - [ ] IO time
  - [ ] CPU time

## Usage
### Building the container
The container runtime can be built using the Dockerfile available at:
```
/container-runtime/src/main/docker/Dockerfile.jvm
```

Note that the build context for `docker build` should be at `/container-runtime`

The following command can be used from the build context:
```
docker build -t container-runtime . -f ./container-runtime/src/main/docker/Dockerfile.jvm
```

### Running the container
The container runtime can be run using the following docker command:
```
docker run -v /var/run/docker.sock:/var/run/docker.sock -v /tmp/themis:/tmp/themis -v /sys/fs/cgroup/docker:/docker/cgroups -i -p 9000:9000 --user root --privileged {image}
```

## Design
### Modules
The overall project consists of 3 modules:
- `runtime`: The main module that hosts the gRPC server and runs the containerized workloads.
- `process-monitor`: A module that receives information from the container about its internal processes and resource usage.
- `integration-tester`: A module that can run end-to-end integration tests on the container runtime. This module is not packaged into the container runtime service.

### Higher-level architecture
The architecture of the container runtime service is as follows:

```mermaid
flowchart TB
  gRPC["gRPC client"]
  subgraph Container-runtime service
    A["Runtime"]
    B["Process-monitor"]
  end
  gVisor["gVisor container"]

  gRPC --> A
  A --Docker SDK--> gVisor
  gVisor --uds://tmp/themis/process_monitor.sock --> B
  A --Internal function calls--> B
```

### Process monitor
The process monitor talks with gVisor over a protocol called `Sequenced Socket (SOCK_SEQPACKET)`, which makes use of the unix domain socket.
The process monitor listens to this socket as a server. gVisor sends update about system calls happening within the container to this socket.
These system calls are then used to track the state of the container and its processes. The container runtime module will eventually query this state through a function call.

### Runtime
The container runtime listens as a gRPC server, implementing the judgement communication spec (https://gitlab.com/rug-digitallab/products/themis/runtimes/communication-spec).
Once a request is received, the container runtime will execute a set of docker commands to spin up a container according to the specification of the request.
The container runtime will then wait for the container to finish, and return the result along with metrics.

The main execution process for starting a container is located at `nl.rug.digitallab.themis.runtime.container.runner.ContainerRunner`.

While ideally we want to collect results and metrics in one go after the execution of the container,
sometimes we need to asynchronously monitor and collect data to determine results. This is true with for example collecting the output stream of the container.
To handle this, a set of "monitors" run asynchronously to the main execution process. These monitors are located in the `nl.rug.digitallab.themis.runtime.container.runner.monitors` package.

## Integration tests
The container runtime has a set of integration tests to test the full container runtime against a set of common actions.
These integration tests present themselves as unit tests, allowing for integration with other tools, but are based on a custom setup.

The testcases are located in `/tests` within the integration-tester module. Any folder detected there will be run as a testcase.
This folder is expected to have a `testcase.yaml` file that describes the testcase. This yaml has a `input` field and an `expectedOutput` field.
The object within those fields are directly parsed as the protobuf request and response objects respectively.
The `expectedOutput` field should be an exact match with the object produced by the container runtime.

There are multiple integration test "environments". An environment is essentially the setup of the container runtime that the testcases are run against.
An environment is spun up as a testcontainer Quarkus resource, which will be started prior to test execution, and shut down post-execution.
To use a specific environment, the name of the environment should be specified in `digital-lab.integration-tester.environment`
in the `application.yaml` file within the integration test module.

The current available environments are:
- `containerised`: A docker-in-docker setup that fully sets up the container runtime infrastructure within a container,
  within which the container-runtime and the executed actions will run. This is the simplest to run, requiring only docker to be installed.
  However, it is much slower and less representative of a production environment. 
- `native`: A setup that runs the container runtime directly on the host machine. 
  This is much faster and more representative of a production environment,
  but requires the host machine to have the necessary configurations to run the container runtime.

Since `native` is quite cumbersome to get running, it is recommended to use `containerised` for development purposes.

If you want to set up the native integration environment, please refer to `Requirements` section of this README.
