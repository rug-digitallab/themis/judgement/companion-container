plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val sequencedSocketVersion: String by project
    val communicationSpecVersion: String by project
    val kotlinMockitoVersion: String by project
    val coroutineTestVersion: String by project

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.sequencedsocket:sequenced-socket:$sequencedSocketVersion")

    // Quarkus dependencies
    implementation("io.quarkus:quarkus-hibernate-validator")
    implementation("io.quarkus:quarkus-grpc")

    testImplementation("io.quarkus:quarkus-junit5-mockito")
    testImplementation("org.mockito.kotlin:mockito-kotlin:$kotlinMockitoVersion")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:$coroutineTestVersion")
}

tasks.withType<Jar> {
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
