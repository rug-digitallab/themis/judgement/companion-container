package nl.rug.digitallab.themis.runtime.container.monitor.gvisor

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.ProtoHeader
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [ProtoHeader].
 */
@QuarkusTest
class ProtoHeaderTest {
    @Test
    fun `The ProtoHeader correctly parses a byte array based on the gVisor specification`() {
        val protoHeader = ProtoHeader.fromByteArray(
            byteArrayOf(8, 0, 2, 0, 4, 0, 0, 0)
        )
        assertEquals(protoHeader.headerSize, 8)
        assertEquals(protoHeader.messageType, 2)
        assertEquals(protoHeader.droppedCount, 4)
    }
}
