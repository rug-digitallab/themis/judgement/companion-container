package nl.rug.digitallab.themis.runtime.container.monitor.gvisor

import gvisor.common.Common.Handshake
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import kotlinx.coroutines.runBlocking
import nl.rug.digitallab.sequencedsocket.interfaces.SequencedClient
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitorConfig
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.ConnectionFinishedException
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorClient
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.kotlin.any
import org.mockito.kotlin.whenever

/**
 * Tests for [GVisorClient].
 */
@QuarkusTest
class GVisorClientTest {
    @Inject
    private lateinit var config: ProcessMonitorConfig

    /**
     * Sets up a mocked client that can be used for testing the handshake.
     */
    fun setupMockedHandshakeClient(): SequencedClient {
        val mockedClient: SequencedClient = mock()

        runBlocking {
            whenever(mockedClient.read(any(), any())).thenReturn(
                Handshake.newBuilder()
                    .setVersion(1)
                    .build().toByteArray()
            )

            whenever(mockedClient.write(any())).then {
                val message = it.getArgument<ByteArray>(0)
                assertEquals(
                    1, Handshake.parseFrom(message).version
                )

                return@then 0
            }
        }

        return mockedClient
    }

    /**
     * Sets up a mocked client that can be used for testing the read function.
     *
     * @param buffer The buffer to return when the client is read from.
     */
    fun setupMockedReadClient(buffer: ByteArray): SequencedClient {
        val mockedClient: SequencedClient = mock()

        runBlocking {
            whenever(mockedClient.read(any(), any())).thenReturn(buffer)
        }

        return mockedClient
    }

    @Test
    fun `The GVisorClient properly sends a handshake response`() {
        val mockedClient: SequencedClient = setupMockedHandshakeClient()

        runBlocking {
            val client = GVisorClient(mockedClient, config)
            client.handshake()

            // Verify that the handshake was sent
            Mockito.verify(mockedClient).write(any())
        }
    }

    @Test
    fun `The GVisorClient correctly parses the message header`() {
        val mockedClient: SequencedClient = setupMockedReadClient(
             byteArrayOf(8, 0, 1, 0, 2, 0, 0, 0, 1, 2)
        )

        runBlocking {
            val client = GVisorClient(mockedClient, config)
            val message = client.readProtoMessage()

            assertEquals(8, message.header.headerSize)
            assertEquals(1, message.header.messageType)
            assertEquals(2, message.header.droppedCount)

            assertArrayEquals(byteArrayOf(1, 2), message.body)
        }
    }
    
    @Test
    fun `The GVisorClient correctly handles the conection termination message`() {
        val mockedClient: SequencedClient = setupMockedReadClient(byteArrayOf())

        runBlocking {
            val client = GVisorClient(mockedClient, config)
            assertThrows<ConnectionFinishedException> { client.readProtoMessage() }
        }
    }
}
