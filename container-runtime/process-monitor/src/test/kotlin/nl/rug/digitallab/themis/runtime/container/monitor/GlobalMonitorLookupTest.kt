package nl.rug.digitallab.themis.runtime.container.monitor

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.GlobalMonitorLookup
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitor
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitorConfig
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [GlobalMonitorLookup].
 */
@QuarkusTest
class GlobalMonitorLookupTest {
    @Inject
    private lateinit var config: ProcessMonitorConfig

    @Test
    fun `The GlobalMonitorLookup can correctly store, retrieve and remove monitors`() {
        val monitor = ProcessMonitor(config)
        GlobalMonitorLookup.registerMonitor("test", monitor)
        assertEquals(monitor, GlobalMonitorLookup.getMonitor("test"))
        GlobalMonitorLookup.removeMonitor("test")
        assertEquals(null, GlobalMonitorLookup.getMonitor("test"))
    }
}
