package nl.rug.digitallab.themis.runtime.container.monitor.monitor

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.ByteSizeLongConverter
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.DurationLongConverter
import kotlin.time.Duration

/**
 * Settings for the process monitor.
 *
 * @property maxPacketSize The maximum size of a packet that can be sent to the gVisor container.
 * @property maxWaitForMonitorTermination The maximum amount of time that the process monitor will wait for the container to finish.
 * @property handshakeTimeout The maximum amount of time that the process monitor will wait for a handshake from gVisor.
 * @property readTimeout The maximum amount of time that the process monitor will wait for a response from gVisor.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.process-monitor")
interface ProcessMonitorConfig {
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("max-packet-size")
    fun maxPacketSize(): ByteSize
    @WithConverter(DurationLongConverter::class)
    @WithName("max-wait-for-monitor-termination")
    fun maxWaitForMonitorTermination(): Duration
    @WithConverter(DurationLongConverter::class)
    @WithName("handshake-timeout")
    fun handshakeTimeout(): Duration
    @WithConverter(DurationLongConverter::class)
    @WithName("read-timeout")
    fun readTimeout(): Duration
}
