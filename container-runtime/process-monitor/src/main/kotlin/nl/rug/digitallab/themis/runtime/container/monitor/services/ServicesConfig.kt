package nl.rug.digitallab.themis.runtime.container.monitor.services

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping

@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.container-runtime.services")
interface ServicesConfig {
    fun socketPath(): String
}
