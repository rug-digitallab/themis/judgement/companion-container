package nl.rug.digitallab.themis.runtime.container.monitor.monitor

import gvisor.common.Common.MessageType
import gvisor.container.Container.Start
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withTimeout
import nl.rug.digitallab.sequencedsocket.exceptions.TimedOutException
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.ConnectionFinishedException
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorClient
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorMessage
import org.jboss.logging.Logger


/**
 * A monitor for processes within a gVisor container.
 * The messages received are from the gVisor runtime monitoring subsystem.
 *
 * @param config The configuration properties for the process monitor.
 */
class ProcessMonitor(
    private val config: ProcessMonitorConfig,
) {
    private val logger = Logger.getLogger(ProcessMonitor::class.java)
    // Tracks the current amount of processes running within the container
    private var userProcessCount = 1
    // Tracks the maximum amount of processes that were running at the same time
    // Used to determine whether the container exceeded the maximum amount of processes during the run
    var maxUserProcessCount = 1
        private set
    // Tracks the total amount of processes created within the container
    var totalCreatedProcesses = 1
        private set
    // Tracks whether the container has finished running
    private val mutex = Mutex()
    // ID of the container
    private var containerId = "NO_ID_SET"
    // Extra safety guarantee to ensure resources get cleaned up, even if the process monitor fails.
    // This boolean controls whether the read loop should continue. The process monitor will periodically time out to check this value.
    // As long as this value is set to true, and a reasonable timeout value is set in the config, the process monitor will eventually finish.
    private var containerFinished = false
    // Whether the monitor encountered an exception while monitoring the container
    private var monitorException: Throwable? = null

    /**
     * Monitors for processes within a gVisor container.
     *
     * @param containerSocket The socket that gVisor will send messages to.
     */
    suspend fun monitorContainer(containerSocket: GVisorClient) {
        mutex.withLock {
            try {
                containerSocket.use {socket ->
                    // Perform a handshake with the gVisor container
                    socket.handshake()
                    // Receive messages from the gVisor container monitor and process them
                    while (!containerFinished) {
                        try {
                            // Synchronously read the next message sent from gVisor
                            val message = socket.readProtoMessage()

                            // Handle the message according to the header information
                            processProtoMessage(message)
                        } catch(e: ConnectionFinishedException) {
                            logger.info("Process monitor: Monitor for container with ID $containerId finished.")
                            break
                        } catch(e: TimedOutException) {
                            logger.debug("Process monitor: Did not receive a message from container with ID $containerId within the last ${config.readTimeout()} milliseconds.")
                        }
                    }
                }
            } catch (e: Throwable) {
                monitorException = e
                logger.error("Process monitor: Monitor for container with ID $containerId encountered an exception: $e")
            }
        }
    }

    /**
     * Checks whether the container has finished running.
     * This function will block for a configurable amount of time while waiting for the container to finish.
     * This time can be configured through the "process-monitor.max-wait-for-monitor-termination-ms" property.
     *
     * Normally, the process monitor should already have finished by the time this function is called.
     */
    suspend fun awaitFinish() {
        try {
            withTimeout(config.maxWaitForMonitorTermination()) {
                mutex.withLock {}
            }
        } finally {
            // Pass any exception encountered to the gRPC code flow so the gRPC caller sees it
            if (monitorException != null) throw monitorException!!

            // Ensure the process monitor stops reading messages from gVisor
            containerFinished = true

            // Clean up the process monitor
            GlobalMonitorLookup.removeMonitor(containerId)
        }
    }

    /**
     * Processes a message received from gVisor.
     * It separates the header from the message and processes the message based on the header.
     *
     * @param message The message received from gVisor.
     */
    private fun processProtoMessage(message: GVisorMessage) {
        when(MessageType.forNumber(message.header.messageType.toInt())) {
            MessageType.MESSAGE_CONTAINER_START  -> processStart(Start.parseFrom(message.body))
            MessageType.MESSAGE_SENTRY_CLONE     -> processCloneInfo()
            MessageType.MESSAGE_SENTRY_TASK_EXIT -> processTaskExit()
            else -> throw IllegalArgumentException("Unknown message type: ${message.header.messageType}")
        }
    }

    /**
     * Processes a start message from gVisor. Used as the initialisation point for the monitor.
     * This function is responsible for registering the monitor with the global lookup that the main gRPC server uses.
     *
     * @param start The start message from gVisor.
     */
    private fun processStart(start: Start) {
        logger.info("Process monitor: Monitor for container with id ${start.id} started.")

        containerId = start.id
        GlobalMonitorLookup.registerMonitor(start.id, this)
    }

    /**
     * Processes a clone info message, which indicates a new process has been created.
     */
    private fun processCloneInfo() {
        userProcessCount++
        totalCreatedProcesses++
        maxUserProcessCount = maxOf(userProcessCount, maxUserProcessCount)
    }

    /**
     * Processes a task exit message, which indicates a process has exited.
     */
    private fun processTaskExit() {
        userProcessCount--
    }
}
