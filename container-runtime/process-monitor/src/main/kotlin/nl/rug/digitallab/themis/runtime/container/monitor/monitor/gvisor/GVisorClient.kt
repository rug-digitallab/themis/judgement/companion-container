package nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor

import gvisor.common.Common.Handshake
import nl.rug.digitallab.sequencedsocket.exceptions.TimedOutException
import nl.rug.digitallab.sequencedsocket.interfaces.SequencedClient
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitorConfig

/**
 * A client for communicating with a gVisor container.
 *
 * @param client The sequenced client to use for communication with the gVisor container.
 * @param config The configuration properties for the process monitor.
 */
open class GVisorClient(
    client: SequencedClient,
    private val config: ProcessMonitorConfig,
) : SequencedClient by client {

    /**
     * Performs the initiation handshake with the gVisor container.
     *
     * @throws IllegalArgumentException When gVisor sends an unsupported handshake version.
     * @throws TimedOutException When the handshake timed out.
     */
    @Throws(IllegalArgumentException::class, TimedOutException::class)
    open suspend fun handshake() {
        // Receive the initial handshake message from gVisor
        val handshakeMessage = this.read(
            readSize = config.maxPacketSize().B.toInt(),
            timeout = config.handshakeTimeout().inWholeMilliseconds.toInt(),
        )

        val handshakeIn = Handshake.parseFrom(handshakeMessage)
        require(handshakeIn.version == 1) { "Unsupported handshake version sent by gVisor: ${handshakeIn.version}" }

        // Send the responding handshake message to gVisor
        val handshakeOut = Handshake.newBuilder()
            .setVersion(1)
            .build()
        this.write(handshakeOut.toByteArray())
    }

    /**
     * Reads the next message sent by gVisor.
     * Handles the parsing of the header and the separation of the header and the message.
     *
     * @return The next message sent by gVisor.
     *
     * @throws ConnectionFinishedException When gVisor indicates that the last message was sent.
     * @throws TimedOutException When the read operation timed out.
     */
    @Throws(ConnectionFinishedException::class, TimedOutException::class)
    open suspend fun readProtoMessage(): GVisorMessage {
        // Synchronously read the next message sent from gVisor
        val buffer = this.read(
            readSize = config.maxPacketSize().B.toInt(),
            timeout = config.readTimeout().inWholeMilliseconds.toInt(), // Time out regularly to check whether the container has finished
        )

        // gVisor sends a zero-length message when the container exits
        if(buffer.isEmpty())
            throw ConnectionFinishedException("gVisor indicated that the last message was sent")

        val header = ProtoHeader.fromByteArray(buffer)
        val message = buffer.copyOfRange(header.headerSize.toInt(), buffer.size)
        return GVisorMessage(header, message)
    }
}
