package nl.rug.digitallab.themis.runtime.container.monitor.monitor

import org.jboss.logging.Logger

/**
 * A lookup table for process monitors.
 */
object GlobalMonitorLookup {
    private val logger = Logger.getLogger("GlobalMonitorLookup")

    private val monitorsLookup = mutableMapOf<String, ProcessMonitor>()

    /**
     * Gets the process monitor attached to a container.
     *
     * @param containerId The ID of the container to get the monitor for.
     *
     * @return The process monitor for the container, or null if it does not exist.
     */
    fun getMonitor(containerId: String): ProcessMonitor? {
        return monitorsLookup[containerId]
    }

    /**
     * Registers a process monitor for a container.
     *
     * @param containerId The ID of the container to register the monitor for.
     * @param monitor The process monitor to register.
     */
    fun registerMonitor(containerId: String, monitor: ProcessMonitor) {
        monitorsLookup[containerId] = monitor
    }

    /**
     * Removes a process monitor for a container from the lookup.
     *
     * @param containerId The ID of the container to remove the attached monitor for.
     */
    fun removeMonitor(containerId: String) {
        val removedMonitor = monitorsLookup.remove(containerId)

        if (removedMonitor == null)
            logger.warn("Tried to remove monitor for container $containerId, but no monitor was attached.")
    }
}
