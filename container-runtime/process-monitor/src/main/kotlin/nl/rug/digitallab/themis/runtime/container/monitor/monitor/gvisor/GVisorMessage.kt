package nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor

/**
 * A class representing a full message sent back by a gVisor container.
 *
 * @property header The header of the message.
 * @property body The body of the message. This is a valid protobuf message encoded as a byte array.
 */
data class GVisorMessage(
    val header: ProtoHeader,
    val body: ByteArray,
) {
    /**
     * Checks for equality of two GVisorMessages.
     * Should be overridden due to using an array as a property.
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GVisorMessage) return false

        if (header != other.header) return false
        if (!body.contentEquals(other.body)) return false

        return true
    }

    /**
     * Calculates the hash code of the GVisorMessage.
     * Should be overridden due to using an array as a property.
     */
    override fun hashCode(): Int {
        var result = header.hashCode()
        result = 31 * result + body.contentHashCode()
        return result
    }
}
