package nl.rug.digitallab.themis.runtime.container.monitor.services

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import kotlinx.coroutines.*
import nl.rug.digitallab.sequencedsocket.DefaultSequencedServer
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitor
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.ProcessMonitorConfig
import nl.rug.digitallab.themis.runtime.container.monitor.monitor.gvisor.GVisorClient
import org.jboss.logging.Logger

/**
 * A service for handling the messages sent by gVisor's syscall monitor.
 * These messages are used by the process monitor to keep track of the container's internal processes.
 */
@ApplicationScoped
class ProcessService {
    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var config: ServicesConfig

    @Inject
    private lateinit var monitorConfig: ProcessMonitorConfig

    /**
     * Starts the service listening for gVisor syscall monitor connections
     */
    suspend fun startService() {
        val server = DefaultSequencedServer()

        logger.info("Started process monitor service on ${config.socketPath()}")
        server.start(config.socketPath())

        while (true) {
            val socket = server.accept()
            CoroutineScope(Dispatchers.IO).launch {
                // Create a new process monitor for each connection, so it can keep track of the processes in that container
                ProcessMonitor(monitorConfig)
                    .monitorContainer(GVisorClient(socket, monitorConfig))
            }
        }
    }
}
