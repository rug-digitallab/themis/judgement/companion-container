package nl.rug.digitallab.themis.runtime.common.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.test.util.compareFilePermissions
import org.instancio.Instancio
import org.junit.jupiter.api.Test

@QuarkusTest
class ActionResourcePermissionsTest {
    @Test
    fun `A proto permissions is properly parsed to an internal permissions representation`() {
        val protoPerms = Instancio.create(nl.rug.protospec.judgement.v1.Permissions::class.java)
        val containerPerms = ActionResourcePermissions.fromProto(protoPerms)

        compareFilePermissions(protoPerms, containerPerms)
    }
}
