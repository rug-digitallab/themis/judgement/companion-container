package nl.rug.digitallab.themis.runtime.common.services

import io.quarkus.grpc.GrpcService
import io.quarkus.test.junit.QuarkusTest
import jakarta.enterprise.context.ApplicationScoped
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.common.runner.ActionRunner
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionExecutionMetrics
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionResult
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.protospec.judgement.v1.executionLimits
import nl.rug.protospec.judgement.v1.extensions.wallTime
import nl.rug.protospec.judgement.v1.runActionRequest
import org.junit.jupiter.api.*
import java.time.Duration
import kotlin.time.Duration.Companion.seconds

/**
 * Tests for [GrpcService].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class GrpcServiceTest {
    @GrpcService
    lateinit var service: nl.rug.digitallab.themis.runtime.common.services.GrpcService

    @Test
    fun `The ActionService correctly deals with an invalid gRPC request`() {
        assertThrows<IllegalArgumentException> {
            service.runAction(null).await().atMost(Duration.ofSeconds(1))
        }
    }

    @Test
    fun `The ActionService correctly deals with a valid gRPC request`() {
        assertDoesNotThrow {
            service.runAction(
                runActionRequest {
                    limits = executionLimits {
                        wallTime = 1.seconds
                        cpuCores = 1.0f
                        actionProcesses = 50
                        userProcesses = 1
                        memoryBytes = 50.MB.B
                        diskBytes = 50.MB.B
                    }
                }
            ).await().atMost(Duration.ofSeconds(1))
        }
    }
}

@ApplicationScoped
class MockedRunner : ActionRunner {
    override suspend fun runAction(configuration: ActionConfiguration): ActionResult {
        return ActionResult(ActionStatus.SUCCEEDED, 0, listOf(), ActionExecutionMetrics())
    }
}
