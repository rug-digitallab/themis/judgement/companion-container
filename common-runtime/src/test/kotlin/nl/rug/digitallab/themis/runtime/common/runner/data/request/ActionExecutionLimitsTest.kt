package nl.rug.digitallab.themis.runtime.common.runner.data.request

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.KB
import nl.rug.digitallab.common.kotlin.quantities.MB
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

/**
 * Tests for [ActionExecutionLimits].
 */
@QuarkusTest
class ActionExecutionLimitsTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @ParameterizedTest(name = "Limits are properly built for a wall time of {0} milliseconds")
    @ValueSource(ints = [100, 1000, 5000, 10000])
    fun `Limits are properly built for various walltime values`(wallTime: Int) {
        assertDoesNotThrow {
            ActionExecutionLimits(
                wallTime = wallTime.milliseconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a cpu cores value of {0}")
    @ValueSource(floats = [0.1f, 0.5f, 1.0f, 2.0f])
    fun `Limits are properly built for various cpu cores values`(cores: Float) {
        assertDoesNotThrow {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = cores,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a container processes value of {0}")
    @ValueSource(ints = [50, 100, 200])
    fun `Limits are properly built for various container processes values`(processes: Int) {
        assertDoesNotThrow {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = processes,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a user processes of {0}")
    @ValueSource(ints = [5, 10, 20])
    fun `Limits are properly built for various user processes values`(processes: Int) {
        assertDoesNotThrow {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = processes,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are properly built for a memory of {0}")
    @ValueSource(longs = [10000, 50000, 100000, 200000])
    fun `Limits are properly built for various memory values`(memory: Long) {
        assertDoesNotThrow {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = memory.KB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for a bad wall time value of {0} milliseconds")
    @ValueSource(ints = [0, 10, 50, 99])
    fun `Limits are not built for various bad walltime values`(wallTime: Int) {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = wallTime.milliseconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for a bad cpu core value of {0}")
    @ValueSource(floats = [0.0f, 0.01f, 0.09f, 0.099f])
    fun `Limits are not built for various bad cpu core values`(cores: Float) {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = cores,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @Test
    fun `Limits are not built for a bad container processes value`() {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 0,
                userProcesses = 5,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @Test
    fun `Limits are not built for a bad user processes value`() {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 0,
                memory = 50.MB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for a bad memory value of {0}")
    @ValueSource(longs = [0, 10, 100, 1000, 9999])
    fun `Limits are not built for various bad memory values`(memory: Long) {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = memory.KB,
                disk = 50.MB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }

    @ParameterizedTest(name = "Limits are not built for a bad disk value of {0}")
    @ValueSource(longs = [0, 10, 100, 1000, 9999])
    fun `Limits are not built for various bad disk values`(disk: Long) {
        assertThrows<IllegalArgumentException> {
            ActionExecutionLimits(
                wallTime = 1.seconds,
                cpuCores = 1.0f,
                actionProcesses = 50,
                userProcesses = 5,
                memory = 50.MB,
                disk = disk.KB,
                output = 5.MB,
                limitsConfig = limitsConfig,
            )
        }
    }
}
