package nl.rug.digitallab.themis.runtime.common.runner.data.request

import io.quarkus.test.junit.QuarkusTest
import nl.rug.protospec.judgement.v1.extensions.uri
import org.instancio.Instancio
import org.junit.jupiter.api.Test

/**
 * Tests the [ActionOutputRequest] class.
 */
@QuarkusTest
class ActionOutputRequestsTest {
    @Test
    fun `A proto OutputFileRequest is properly parsed to an internal OutputFileRequest`() {
        val proto = Instancio
            .of(nl.rug.protospec.judgement.v1.OutputRequest::class.java)
            .create()

        val request = ActionOutputRequest.fromProto(proto)

        assert(request.uri == proto.uri)
        assert(request.maxSize.B == proto.maxSizeBytes)
    }
}
