package nl.rug.digitallab.themis.runtime.test.util

import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import kotlin.time.Duration.Companion.seconds

fun getBasicContainerLimits(config: ResourceLimitsConfig): ActionExecutionLimits {
    return ActionExecutionLimits(
        wallTime = 1.seconds,
        cpuCores = 1.0f,
        actionProcesses = 50,
        userProcesses = 5,
        memory = 50.MB,
        disk = 50.MB,
        output = 5.MB,
        limitsConfig = config,
    )
}
