package nl.rug.digitallab.themis.runtime.common.runner.contexts

import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.test.util.getBasicContainerLimits
import org.instancio.Instancio
import org.jboss.logging.Logger
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

/**
 * Tests for [ActionContext].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_METHOD)
class ContainerContextTest {
    @Inject
    private lateinit var config: ResourceLimitsConfig

    val context by lazy {
        ActionContext("testContainerId", Logger.getLogger("testLogger"), getBasicContainerLimits(config))
    }

    @Test
    fun `The context throws an error if the status is accessed before it is set`() {
        assertThrows(UninitializedPropertyAccessException::class.java) {
            context.actionExecStatus
        }
    }

    @ParameterizedTest(name = "The context can take a valid status and store it: {0}")
    @EnumSource(ActionStatus::class)
    fun `The context can take a valid status and store it`(status: ActionStatus) {
        context.reportStatus(status)
        assertEquals(status, context.actionExecStatus)
    }

    @Test
    fun `The context properly enforces the priority of the statuses`() {
        // Report 5 random statuses
        Instancio.ofList(ActionStatus::class.java)
            .size(5)
            .create()
            .forEach(context::reportStatus)

        // Highest priority status reported
        context.reportStatus(ActionStatus.EXCEEDED_WALL_TIME)

        assertEquals(ActionStatus.EXCEEDED_WALL_TIME, context.actionExecStatus)
    }

    @Test
    fun `The container id is properly stored`() {
        assertEquals("testContainerId", context.resourceId)
    }

    @Test
    fun `The exit code is properly stored`() {
        context.reportExitCode(25)
        assertEquals(25, context.exitCode)
    }
}
