package nl.rug.digitallab.themis.runtime.common.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.net.URI

/**
 * Tests for [ActionOutput].
 */
@QuarkusTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ActionOutputTest {
    @Test
    fun `An internal OutputFile is properly parsed to a proto OutputFile`() {
        val output: ActionOutput = Instancio.create(ActionOutput::class.java)
        val protoOutput = output.toProto()

        assertEquals(protoOutput.resource, output.resource.toProto())
        assertEquals(protoOutput.isPresent, output.isPresent)
        assertEquals(protoOutput.exceededSize, output.sizeExceeded)
    }

    @Test
    fun `Correct implementation of non-present files`() {
        val file = ActionOutput.nonPresent(URI("file:test"))

        assertFalse(file.isPresent)
        assertFalse(file.sizeExceeded)
        assertEquals(URI("file:test"), file.resource.uri)
    }

    @Test
    fun `Correct implementation of files that are too large`() {
        val file = ActionOutput.tooLarge(URI("file:test"))

        assertTrue(file.isPresent)
        assertTrue(file.sizeExceeded)
        assertEquals(URI("file:test"), file.resource.uri)
    }

    @Test
    fun `A resource can be created from an output stream`() {
        val file = ActionOutput.fromOutStream("test", "test".toByteArray())

        assertTrue(file.isPresent)
        assertFalse(file.sizeExceeded)
        assertEquals(URI("stream:test"), file.resource.uri)
        assertEquals("test", String(file.resource.content))
    }
}
