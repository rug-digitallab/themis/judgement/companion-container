package nl.rug.digitallab.themis.runtime.common.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.extensions.uri
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [ActionResource].
 */
@QuarkusTest
class ActionResourceTest {
    @Test
    fun `A proto file is properly parsed to an internal file representation`() {
        val protoFile = Instancio.of(Resource::class.java)
            .create()
        val actionResource = ActionResource.fromProto(protoFile)

        assertEquals(protoFile.uri, actionResource.uri)
        assertArrayEquals(protoFile.contents.toByteArray(), actionResource.content)
        assertEquals(ActionResourceMetadata.fromProto(protoFile.meta), actionResource.metadata)
    }
}
