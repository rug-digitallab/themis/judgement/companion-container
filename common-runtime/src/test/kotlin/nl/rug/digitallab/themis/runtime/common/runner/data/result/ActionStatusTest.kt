package nl.rug.digitallab.themis.runtime.common.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource

/**
 * Tests for [ActionStatus].
 */
@QuarkusTest
class ActionStatusTest {
    @ParameterizedTest(name = "The status {0} is properly parsed to a proto status")
    @EnumSource(ActionStatus::class)
    fun `An internal status is properly parsed to a proto status`(status: ActionStatus) {
        val protoStatus = status.toProto()
        assertEquals(status.name, protoStatus.name)
    }

    @Test
    fun `A logical priority list is set up for the statuses`() {
        assertTrue(ActionStatus.SUCCEEDED.toPriority() < ActionStatus.CRASHED.toPriority())
        assertTrue(ActionStatus.PRODUCED_ORPHANS.toPriority() < ActionStatus.PRODUCED_ZOMBIES.toPriority())
        assertTrue(ActionStatus.PRODUCED_ZOMBIES.toPriority() < ActionStatus.PRODUCED_ZOMBIES_AND_ORPHANS.toPriority())
        assertTrue(ActionStatus.PRODUCED_ZOMBIES_AND_ORPHANS.toPriority() < ActionStatus.CRASHED.toPriority())
        assertTrue(ActionStatus.CRASHED.toPriority() < ActionStatus.EXCEEDED_PROCESSES.toPriority())
        assertTrue(ActionStatus.EXCEEDED_PROCESSES.toPriority() < ActionStatus.EXCEEDED_MEMORY.toPriority())
        assertTrue(ActionStatus.EXCEEDED_MEMORY.toPriority() < ActionStatus.EXCEEDED_WALL_TIME.toPriority())
    }
}
