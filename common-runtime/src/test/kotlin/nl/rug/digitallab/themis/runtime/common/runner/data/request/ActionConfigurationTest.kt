package nl.rug.digitallab.themis.runtime.common.runner.data.request

import com.google.protobuf.ByteString
import io.quarkus.test.junit.QuarkusTest
import jakarta.inject.Inject
import nl.rug.digitallab.common.kotlin.quantities.MB
import nl.rug.protospec.judgement.v1.*
import nl.rug.protospec.judgement.v1.extensions.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.net.URI
import kotlin.time.Duration.Companion.seconds

/**
 * Tests for [ActionConfiguration].
 */
@QuarkusTest
class ActionConfigurationTest {
    @Inject
    lateinit var limitsConfig: ResourceLimitsConfig

    @Test
    fun `A proto container configuration is properly parsed to an internal file container configuration`() {
        val protoRequest = runActionRequest {
            environment = "ubuntu"
            command += "cat"
            command += "test.txt"
            limits = executionLimits {
                wallTime = 5.seconds
                cpuCores = 1f
                actionProcesses = 50
                userProcesses = 5
                memory = 100.MB
                disk = 100.MB
                output = 100.MB
            }
            inputs += resource {
                uri = URI("file:test.txt")
                contents = ByteString.copyFromUtf8("This is the content of a file")
            }
        }

        val containerConfig = ActionConfiguration.fromProto(protoRequest, limitsConfig)

        assertEquals(protoRequest.commandList, containerConfig.command)
        assertEquals(protoRequest.environment, containerConfig.image)
        assertEquals(protoRequest.limits.cpuCores, containerConfig.executionLimits.cpuCores)
        assertEquals(protoRequest.limits.disk, containerConfig.executionLimits.disk)
        assertEquals(protoRequest.limits.output, containerConfig.executionLimits.output)
        assertEquals(protoRequest.limits.memory, containerConfig.executionLimits.memory)
        assertEquals(protoRequest.limits.actionProcesses, containerConfig.executionLimits.actionProcesses)
        assertEquals(protoRequest.limits.wallTime, containerConfig.executionLimits.wallTime)
        assertEquals(protoRequest.envVarsMap.size, containerConfig.envVars.size)
        assertEquals(protoRequest.inputsList.size, containerConfig.inputs.size)
    }
}
