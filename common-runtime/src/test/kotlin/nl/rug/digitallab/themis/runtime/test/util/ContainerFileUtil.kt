package nl.rug.digitallab.themis.runtime.test.util

import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourcePermissions
import nl.rug.protospec.judgement.v1.Permissions
import org.junit.jupiter.api.Assertions

/**
 * Compares the permissions of a proto file permissions and a container file permissions.
 *
 * @param protoPerms The proto file permissions.
 * @param containerPerms The container file permissions.
 */
fun compareFilePermissions(protoPerms: Permissions, containerPerms: ActionResourcePermissions) {
    Assertions.assertEquals(protoPerms.execute, containerPerms.execute)
    Assertions.assertEquals(protoPerms.read, containerPerms.read)
    Assertions.assertEquals(protoPerms.write, containerPerms.write)
}
