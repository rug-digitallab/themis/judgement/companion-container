package nl.rug.digitallab.themis.runtime.common.runner.data.result

import io.quarkus.test.junit.QuarkusTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

/**
 * Tests for [ActionResult].
 */
@QuarkusTest
class ActionResultTest {
    @Test
    fun `An internal result is properly parsed to a proto result`() {
        val result = ActionResult(
            ActionStatus.SUCCEEDED,
            0,
            listOf(),
            ActionExecutionMetrics(),
        )
        val protoResult = result.toProto()

        assertEquals(result.exitCode, protoResult.result.exitCode)
        assertEquals(result.status.toProto(), protoResult.result.status)
        assertEquals(result.outputs.size, protoResult.result.outputsCount)
    }
}
