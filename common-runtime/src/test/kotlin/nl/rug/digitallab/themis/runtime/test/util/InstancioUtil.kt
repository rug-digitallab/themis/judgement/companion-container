package nl.rug.digitallab.themis.runtime.test.util

import com.google.protobuf.GeneratedMessageV3
import org.instancio.Node
import org.instancio.generator.GeneratorSpec
import org.instancio.generators.Generators
import org.instancio.spi.InstancioServiceProvider

/**
 * Custom generator provider for Instancio, which handles edgecases with Protobuf.
 *
 * Protobuf internally stores strings as Objects, which is a nasty trick to effectively
 * have a union type of String and ByteString - after which the getters convert between
 * the two when needed.
 *
 * This generator provider will find protobuf internal fields that are of type Object
 * and instructs Instancio to use a string generator for those fields. When we return
 * null, no modifications take place and default behaviour continues.
 */
class ProtobufGeneratorProvider : InstancioServiceProvider.GeneratorProvider {
    override fun getGenerator(node: Node, generators: Generators): GeneratorSpec<*>? {
        val field = node.field ?: return null

        // Containing type check
        if(!GeneratedMessageV3::class.java.isAssignableFrom(field.declaringClass))
            return null

        // Check field type
        if(field.type != java.lang.Object::class.java)
            return null

        // Check field name -- we assume here that all protobuf generated internal fields
        // end with an underscore.
        if(!field.name.endsWith("_"))
            return null

        // We assume in this case we have a generated string
        return generators.string()
    }
}

/**
 * Custom service provider for Instancio wrapping some of the custom behaviour
 * we need in this testing suite. It is registered/used by the inclusion in the
 * META-INF file.
 */
class ProtobufServiceProvider : InstancioServiceProvider {
    override fun getGeneratorProvider(): InstancioServiceProvider.GeneratorProvider = ProtobufGeneratorProvider()
}
