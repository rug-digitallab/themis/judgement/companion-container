package nl.rug.digitallab.themis.runtime.common.runner.data.common

import io.quarkus.test.junit.QuarkusTest
import nl.rug.digitallab.themis.runtime.test.util.compareFilePermissions
import nl.rug.protospec.judgement.v1.ResourceMetaData
import org.instancio.Instancio
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

@QuarkusTest
class ActionResourceMetadataTest {
    @Test
    fun `A proto metadata is properly parsed to an internal metadata representation`() {
        val protoFileMeta = Instancio.create(ResourceMetaData::class.java)
        val containerFileMeta = ActionResourceMetadata.fromProto(protoFileMeta)

        Assertions.assertEquals(protoFileMeta.uid, containerFileMeta.uid)
        Assertions.assertEquals(protoFileMeta.gid, containerFileMeta.gid)
        compareFilePermissions(protoFileMeta.userPermissions, containerFileMeta.userPermissions)
        compareFilePermissions(protoFileMeta.groupPermissions, containerFileMeta.groupPermissions)
        compareFilePermissions(protoFileMeta.othersPermissions, containerFileMeta.othersPermissions)
    }

    @Test
    fun `The correct posix integer value is generated from the resource permissions`() {
        val perms = ActionResourceMetadata(
            1,
            2,
            userPermissions = ActionResourcePermissions(read = true, write = true, execute = true),
            groupPermissions = ActionResourcePermissions(read = true, write = false, execute = true),
            othersPermissions = ActionResourcePermissions(read = true, write = false, execute = false)
        )
        val posix = perms.posixPermissions

        Assertions.assertEquals(492, posix)
    }
}
