package nl.rug.digitallab.themis.runtime.common.runner.data.common

import nl.rug.protospec.judgement.v1.ResourceMetaData
import nl.rug.protospec.judgement.v1.resourceMetaData

/**
 * The metadata of a resource.
 *
 * @param uid The user id of the resource.
 * @param gid The group id of the resource.
 *
 * @param userPermissions The permissions of the user.
 * @param groupPermissions The permissions of the group.
 * @param othersPermissions The permissions of others.
 */
data class ActionResourceMetadata(
    val uid: Int,
    val gid: Int,
    val userPermissions: ActionResourcePermissions,
    val groupPermissions: ActionResourcePermissions,
    val othersPermissions: ActionResourcePermissions,
) {
    val posixPermissions: Int
        // The first 9 bits of the permission integer are used for the permissions.
        // The first 3 bits are for the user, the next 3 for the group, and the last 3 for others.
        get() =
            (userPermissions.bitmask shl 6) or
            (groupPermissions.bitmask shl 3) or
            (othersPermissions.bitmask shl 0)

    /**
     * [ActionResourceMetadata] constructor that sets its permission using a POSIX permission integer.
     *
     * @param uid The user id of the resource.
     * @param gid The group id of the resource.
     * @param posixPermissions The POSIX permissions of the resource encoded as an integer.
     */
    constructor(
        uid: Int,
        gid: Int,
        posixPermissions: Int,
    ) : this(
        uid = uid,
        gid = gid,
        userPermissions = ActionResourcePermissions(posixPermissions shr 6),
        groupPermissions = ActionResourcePermissions(posixPermissions shr 3),
        othersPermissions = ActionResourcePermissions(posixPermissions shr 0),
    )

    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): ResourceMetaData = resourceMetaData {
        uid = this@ActionResourceMetadata.uid
        gid = this@ActionResourceMetadata.gid
        userPermissions = this@ActionResourceMetadata.userPermissions.toProto()
        groupPermissions = this@ActionResourceMetadata.groupPermissions.toProto()
        othersPermissions = this@ActionResourceMetadata.othersPermissions.toProto()
    }

    companion object {
        /**
         * Convert a protobuf [ResourceMetaData] to a [ActionResourceMetadata].
         *
         * @param permissions The proto resource metadata to convert.
         *
         * @return The converted resource metadata.
         */
        fun fromProto(permissions: ResourceMetaData): ActionResourceMetadata = ActionResourceMetadata(
            uid = permissions.uid,
            gid = permissions.gid,
            userPermissions = ActionResourcePermissions.fromProto(permissions.userPermissions),
            groupPermissions = ActionResourcePermissions.fromProto(permissions.groupPermissions),
            othersPermissions = ActionResourcePermissions.fromProto(permissions.othersPermissions),
        )

        /**
         * Creates an empty metadata object.
         *
         * @return The empty metadata object.
         */
        fun createEmptyMetadata() = ActionResourceMetadata(
            uid = 0,
            gid = 0,
            userPermissions = ActionResourcePermissions.createEmptyPermissions(),
            groupPermissions = ActionResourcePermissions.createEmptyPermissions(),
            othersPermissions = ActionResourcePermissions.createEmptyPermissions(),
        )
    }
}
