package nl.rug.digitallab.themis.runtime.common.runner.data.result

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.executionMetrics
import nl.rug.protospec.judgement.v1.extensions.disk
import nl.rug.protospec.judgement.v1.extensions.memory
import nl.rug.protospec.judgement.v1.extensions.output
import nl.rug.protospec.judgement.v1.extensions.wallTime
import kotlin.time.Duration

/**
 * The metrics collected from the execution of an action.
 *
 * @property wallTime The amount of time the action took to finish.
 * @property memory The amount of memory the action used.
 * @property disk The amount of disk space the action used.
 * @property output The size of the output produced by the action (stdout + stderr).
 * @property createdProcesses The amount of processes created by the action. Only available on gVisor.
 * @property maxProcesses The maximum amount of processes the action had at any given time. Only available on gVisor.
 */
class ActionExecutionMetrics{
    var wallTime: Duration? = null
    var memory: ByteSize? = null
    var disk: ByteSize? = null
    var output: ByteSize? = null
    var createdProcesses: Int? = null
    var maxProcesses: Int? = null

    /**
     * Convert this object to its protobuf equivalent.
     *
     * @return The protobuf equivalent of this object.
     */
    fun toProto() = executionMetrics {
        this@ActionExecutionMetrics.wallTime?.let { this.wallTime = it }
        this@ActionExecutionMetrics.memory?.let { this.memory = it }
        this@ActionExecutionMetrics.disk?.let { this.disk = it }
        this@ActionExecutionMetrics.output?.let { this.output = it }
        this@ActionExecutionMetrics.createdProcesses?.let { this.createdProcesses = it }
        this@ActionExecutionMetrics.maxProcesses?.let { this.maxProcesses = it }
    }
}
