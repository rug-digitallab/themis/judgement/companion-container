package nl.rug.digitallab.themis.runtime.common.runner.data.request

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.ExecutionLimits
import nl.rug.protospec.judgement.v1.extensions.disk
import nl.rug.protospec.judgement.v1.extensions.memory
import nl.rug.protospec.judgement.v1.extensions.output
import nl.rug.protospec.judgement.v1.extensions.wallTime
import kotlin.time.Duration

/**
 * The execution limits for an action.
 * These limits are used to limit the resources an action can use.
 * Used both to protect the system from malicious action, and to evaluate the efficiency of student code.
 * 
 * @param wallTime The maximum amount of time the action can run in real time, represented as a duration.
 * @param cpuCores The maximum amount of CPU cores the action can use. Fractional values are allowed.
 * @param actionProcesses The maximum amount of processes the action can use.
 * @param userProcesses The maximum amount of processes controlled by the user the action can use. (So excludes gVisor processes, etc.)
 * @param memory The maximum amount of memory the action can use.
 * @param disk The maximum amount of disk space the action can use.
 * @param output The maximum amount of stdout + stderr the action can produce.
 */
class ActionExecutionLimits private constructor (
    val wallTime: Duration,
    val cpuCores: Float,
    val actionProcesses: Int,
    val userProcesses: Int,
    val memory: ByteSize,
    val disk: ByteSize,
    val output: ByteSize,
) {
    val internalProcessCount: Int = 1

    /**
     * Check if a value is within the limits of the configuration.
     *
     * @param value The value to check.
     * @param minValue The minimum value the value is allowed to take.
     * @param maxValue The maximum value the value is allowed to take.
     * @param name The name of the value to use in the error message.
     */
    private fun <T : Comparable<T>> checkLimits(value: T, minValue: T, maxValue: T, name: String) {
        require(value >= minValue) { "$name must be at least $minValue" }
        require(value <= maxValue) { "$name must be at most $maxValue" }
    }

    /**
     * Create a new [ExecutionLimits] object.
     *
     * @param wallTime The maximum amount of time the action can run in real time.
     * @param cpuCores The maximum amount of CPU cores the action can use.
     * @param actionProcesses The maximum amount of processes the action can use.
     * @param userProcesses The maximum amount of processes controlled by the user the action can use. (So excludes gVisor processes, etc.)
     * @param memory The maximum amount of memory the action can use.
     * @param disk The maximum amount of disk space the action can use.
     * @param output The maximum amount of stdout + stderr the action can produce.
     * @param limitsConfig The boundary values the limits are allowed to take.
     */
    constructor(
        wallTime: Duration,
        cpuCores: Float,
        actionProcesses: Int,
        userProcesses: Int,
        memory: ByteSize,
        disk: ByteSize,
        output: ByteSize,
        limitsConfig: ResourceLimitsConfig,
    ) : this(
        wallTime = wallTime,
        cpuCores = cpuCores,
        actionProcesses = actionProcesses,
        userProcesses = userProcesses,
        memory = memory,
        disk = disk,
        output = output,
    ) {
        checkLimits(wallTime, limitsConfig.min().wallTime(), limitsConfig.max().wallTime(), "wallTime")
        checkLimits(cpuCores, limitsConfig.min().cpus(), limitsConfig.max().cpus(), "cpuCores")
        checkLimits(actionProcesses, limitsConfig.min().actionProcesses(), limitsConfig.max().actionProcesses(), "actionProcesses")
        checkLimits(userProcesses, limitsConfig.min().userProcesses(), limitsConfig.max().userProcesses(), "userProcesses")
        checkLimits(memory, limitsConfig.min().memory(), limitsConfig.max().memory(), "memory")
        checkLimits(disk, limitsConfig.min().disk(), limitsConfig.max().disk(), "disk")
        checkLimits(output, limitsConfig.min().output(), limitsConfig.max().output(), "output")
    }

    companion object {
        /**
         * Create a new [ExecutionLimits] object from an [ExecutionLimits] object.
         *
         * @param proto The [ExecutionLimits] object to convert.
         * @param config The minimum values for the limits.
         *
         * @return The converted [ExecutionLimits] object.
         */
        fun fromProto(proto: ExecutionLimits, config: ResourceLimitsConfig): ActionExecutionLimits =
            ActionExecutionLimits(
                wallTime = proto.wallTime,
                cpuCores = proto.cpuCores,
                actionProcesses = proto.actionProcesses,
                userProcesses =  proto.userProcesses,
                memory = proto.memory,
                disk = proto.disk,
                output = proto.output,
                limitsConfig = config,
            )
    }
}

