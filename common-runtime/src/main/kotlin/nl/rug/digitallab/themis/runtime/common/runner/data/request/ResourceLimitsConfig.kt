package nl.rug.digitallab.themis.runtime.common.runner.data.request

import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import io.smallrye.config.WithConverter
import io.smallrye.config.WithName
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.ByteSizeLongConverter
import nl.rug.digitallab.common.kotlin.quantities.integrations.smallrye.config.DurationLongConverter
import kotlin.time.Duration

/**
 * A template for setting limits on resources used by an action.
 * It can be used to track either the minimum or maximum values for the limits for example.
 *
 * This interface should not be directly used as a configuration object, but rather as a template for creating one.
 */
interface ResourceLimitsConfigTemplate {
    @WithConverter(DurationLongConverter::class)
    @WithName("wall-time")
    fun wallTime(): Duration
    fun cpus(): Float
    fun actionProcesses(): Int
    fun userProcesses(): Int
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("memory")
    fun memory(): ByteSize
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("disk")
    fun disk(): ByteSize
    @WithConverter(ByteSizeLongConverter::class)
    @WithName("output")
    fun output(): ByteSize
}

/**
 * The configuration for the limits on resources used by an action.
 *
 * This configuration is used to set the minimum and maximum values for the limits on resources used by an action.
 * Minimum values are set as the minimum resources required to be able to run an action. Lower than this would make the action fail independently of what the action actually does.
 * Maximum values are set as the maximum resources that an action may get to protect the infrastructure from overuse.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.runtime.limits")
interface ResourceLimitsConfig {
    fun min(): ResourceLimitsConfigTemplate
    fun max(): ResourceLimitsConfigTemplate
}
