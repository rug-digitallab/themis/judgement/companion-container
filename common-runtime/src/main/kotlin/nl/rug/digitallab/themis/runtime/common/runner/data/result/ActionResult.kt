package nl.rug.digitallab.themis.runtime.common.runner.data.result

import nl.rug.protospec.judgement.v1.RunActionResponse
import nl.rug.protospec.judgement.v1.executionResult
import nl.rug.protospec.judgement.v1.runActionResponse

/**
 * The result of an execution.
 *
 * @param status The status of the execution.
 * @param exitCode The exit code of the execution.
 * @param outputs The resources extracted from the action.
 * @param executionMetrics The resources used by the execution (wall time, memory, etc.).
 */
data class ActionResult(
    val status: ActionStatus,
    val exitCode: Int,
    val outputs: List<ActionOutput>,
    val executionMetrics: ActionExecutionMetrics,
) {
    /**
     * Convert this object to a [RunActionResponse] proto.
     */
    fun toProto(): RunActionResponse =
        runActionResponse {
            result = executionResult {
                status = this@ActionResult.status.toProto()
                exitCode = this@ActionResult.exitCode
                outputs += this@ActionResult.outputs.map { it.toProto() }
            }
            metrics = executionMetrics.toProto()
        }
}
