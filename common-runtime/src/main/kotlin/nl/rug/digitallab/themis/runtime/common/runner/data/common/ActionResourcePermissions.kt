package nl.rug.digitallab.themis.runtime.common.runner.data.common

import nl.rug.protospec.judgement.v1.Permissions
import nl.rug.protospec.judgement.v1.permissions


/**
 * The permissions of a resource.
 *
 * @param read Whether the resource can be read.
 * @param write Whether the resource can be written to.
 * @param execute Whether the resource can be executed.
 */
data class ActionResourcePermissions(
    val read: Boolean,
    val write: Boolean,
    val execute: Boolean,
) {
    val bitmask: Int
        get() =
            (if (read) 1 shl 2 else 0) or
            (if (write) 1 shl 1 else 0) or
            (if (execute) 1 shl 0 else 0)

    /**
     * [ActionResourcePermissions] constructor that sets its permission using a POSIX permission integer.
     *
     * @param bitmask The POSIX permissions of the resource encoded as an integer.
     */
    constructor(
        bitmask: Int,
    ) : this(
        read = (bitmask shr 2) % 2 == 1,
        write = (bitmask shr 1) % 2 == 1,
        execute = (bitmask shr 0) % 2 == 1,
    )

    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): Permissions = permissions {
        read = this@ActionResourcePermissions.read
        write = this@ActionResourcePermissions.write
        execute = this@ActionResourcePermissions.execute
    }

    companion object {
        /**
         * Convert a protobuf Permission class to a [ActionResourcePermissions].
         *
         * @param permissions The gRPC proto permissions to convert.
         *
         * @return The converted permissions.
         */
        fun fromProto(permissions: Permissions): ActionResourcePermissions = ActionResourcePermissions(
            read = permissions.read,
            write = permissions.write,
            execute = permissions.execute,
        )

        /**
         * Creates an empty permission set.
         *
         * @return The empty permission set.
         */
        fun createEmptyPermissions() = ActionResourcePermissions(read = false, write = false, execute = false)
    }
}
