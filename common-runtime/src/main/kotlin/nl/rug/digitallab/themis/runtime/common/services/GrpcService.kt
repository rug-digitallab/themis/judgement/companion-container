package nl.rug.digitallab.themis.runtime.common.services

import io.quarkus.grpc.GrpcService
import io.smallrye.mutiny.Uni
import jakarta.enterprise.inject.Instance
import jakarta.inject.Inject
import kotlinx.coroutines.*
import nl.rug.digitallab.common.quarkus.opentelemetry.contextualUni
import nl.rug.digitallab.themis.runtime.common.runner.ActionRunner
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ResourceLimitsConfig
import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.protospec.judgement.v1.ActionService
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.jboss.logging.Logger

/**
 * The gRPC service for handling the RunAction request from the judgement manager.
 */
@GrpcService
class GrpcService : ActionService {
    @Inject
    private lateinit var runner: Instance<ActionRunner>

    @Inject
    private lateinit var limitsConfig: ResourceLimitsConfig

    @Inject
    private lateinit var logger: Logger

    /**
     * Actually receives the gRPC call. Will execute the action and return the response.
     *
     * @param request The request from the runtime orchestrator.
     *
     * @throws IllegalArgumentException If the request is null.
     *
     * @return The response to the runtime orchestrator.
     */
    @OptIn(ExperimentalCoroutinesApi::class, DelicateCoroutinesApi::class)
    override fun runAction(request: RunActionRequest?): Uni<RunActionResponse> = contextualUni {
        logger.info("Received request:\n$request")

        requireNotNull(request) { "gRPC Request is null" }

        try {
            return@contextualUni runner.get().runAction(
                configuration = ActionConfiguration.fromProto(request, limitsConfig)
            ).toProto()
        } catch (e: Exception) {
            // Uncaught exceptions are not logged by default within a gRPC context, so log them
            logger.error("An error occurred while running gRPC request:\n$request", e)
            // Rethrow such that the gRPC caller will receive the exception
            throw e
        }
    }
}
