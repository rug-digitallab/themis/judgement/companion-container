package nl.rug.digitallab.themis.runtime.common.runner.contexts

import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionExecutionLimits
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionStatus
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionExecutionMetrics
import org.jboss.logging.Logger
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.properties.Delegates

/**
 * Used by the main action runner. It stores data about the action currently running.
 *
 * The idea is that this class can be used to track commonly used data without needing to pass it by parameters.
 * As this class is attached as a Kotlin context receiver, it can be accessed from functions without needing to pass it as a parameter.
 *
 * @property resourceId The ID of the resource attached to this context. Could be a container ID, VM ID or process ID for example.
 * @property logger The logger to use for logging messages.
 * @property executionLimits The resource limits for the execution of the action.
 */
class ActionContext(
    // Parameters for running the action
    val resourceId: String,
    val logger: Logger,
    val executionLimits: ActionExecutionLimits,
) {
    // Variables to store the result of action execution
    lateinit var actionExecStatus: ActionStatus
    var exitCode by Delegates.notNull<Int>()
    val executionMetrics = ActionExecutionMetrics()

    // Output produced by action
    var stdout = byteArrayOf()
    var stderr = byteArrayOf()

    // Flags that track various states of the action
    var outputLimitExceeded = AtomicBoolean(false) // We must keep track of this to reject future output / know we have already killed the process

    /**
     * Reports a status for the action.
     * The status will be marked if it has a higher priority than the current status.
     *
     * @param status The status to mark.
     */
    fun reportStatus(status: ActionStatus) {
        if (
            !::actionExecStatus.isInitialized ||                 // No status has been set yet
            status.toPriority() > actionExecStatus.toPriority()  // The new status has a higher priority than the current status
        ) {
            actionExecStatus = status
            logger.info("Status $status was marked as the new status.")
        } else {
            logger.info("Status $status was not marked as it has a lower priority than the current status: $actionExecStatus")
        }
    }

    /**
     * Reports the exit code of the action.
     *
     * @param exitCode The exit code of the action.
     */
    fun reportExitCode(exitCode: Int) {
        this.exitCode = exitCode
    }
}

