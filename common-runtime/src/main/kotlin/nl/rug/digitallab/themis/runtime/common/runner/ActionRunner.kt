package nl.rug.digitallab.themis.runtime.common.runner

import nl.rug.digitallab.themis.runtime.common.runner.data.request.ActionConfiguration
import nl.rug.digitallab.themis.runtime.common.runner.data.result.ActionResult

/**
 * The base class for implementing the business logic of a runtime.
 *
 * Any concrete implementation of a runtime should implement this runner.
 * The gRPC server is automatically spun up and will call this runner when a request is received.
 */
interface ActionRunner {
    /**
     * Run the action described by the configuration.
     *
     * @param configuration The configuration of the action to run.
     *
     * @return The result of the action.
     */
    suspend fun runAction(
        configuration: ActionConfiguration
    ): ActionResult
}
