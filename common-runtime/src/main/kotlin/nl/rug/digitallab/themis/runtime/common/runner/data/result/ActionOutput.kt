package nl.rug.digitallab.themis.runtime.common.runner.data.result

import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourceMetadata
import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResourcePermissions
import nl.rug.protospec.judgement.v1.Output
import java.net.URI

/**
 * A single file extracted from the action.
 */
data class ActionOutput (
    val resource: ActionResource,
    val isPresent: Boolean,
    val sizeExceeded: Boolean,
) {
    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted proto.
     */
    fun toProto(): Output = Output.newBuilder()
        .setResource(resource.toProto())
        .setIsPresent(isPresent)
        .setExceededSize(sizeExceeded)
        .build()

    companion object {
        /**
         * Convert one of the action's output streams to an [ActionOutput].
         *
         * @param name The name of the stream.
         * @param content The binary content of the stream.
         *
         * @return The stream, represented as an [ActionOutput].
         */
        fun fromOutStream(
            name: String,
            content: ByteArray,
        ) = ActionOutput(
            resource = ActionResource(
                uri = URI("stream:$name"),
                content = content,
                metadata = ActionResourceMetadata(
                    uid = 0,
                    gid = 0,
                    userPermissions = ActionResourcePermissions(read = true, write = false, execute = false),
                    groupPermissions = ActionResourcePermissions(read = true, write = false, execute = false),
                    othersPermissions = ActionResourcePermissions(read = true, write = false, execute = false),
                )
            ),
            isPresent = true,
            sizeExceeded = false,
        )

        /**
         * Creates an [ActionOutput] that represents a resource that was not present in the action.
         *
         * @param uri The URI of the resource.
         *
         * @return The missing resource, represented as an [ActionOutput].
         */
        fun nonPresent(uri: URI) = ActionOutput(
            resource = ActionResource.createEmptyResource(uri),
            isPresent = false,
            sizeExceeded = false,
        )

        /**
         * Creates an [ActionOutput] that represents a resource that was too large to be extracted from the action.
         *
         * @param uri The URI of the file.
         *
         * @return The too large resource, represented as an [ActionOutput].
         */
        fun tooLarge(uri: URI) = ActionOutput(
                resource = ActionResource.createEmptyResource(uri),
                isPresent = true,
                sizeExceeded = true,
            )
    }
}
