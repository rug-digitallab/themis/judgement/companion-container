package nl.rug.digitallab.themis.runtime.common.runner.data.request

import nl.rug.digitallab.themis.runtime.common.runner.data.common.ActionResource
import nl.rug.protospec.judgement.v1.RunActionRequest

/**
 * The configuration data that describes the action that should be run.
 *
 * @param image The image that the action should be executed on.
 * @param executionLimits The resource limits the action should be constrained by.
 * @param envVars The environment variables to insert into the action runtime.
 * @param inputs The resources to insert into the action.
 * @param outputRequests The resources to extract from the action.
 * @param command The command to run by the action. The command's arguments are split into separate strings.
 */
data class ActionConfiguration(
    val image: String,
    val executionLimits: ActionExecutionLimits,
    val envVars: Map<String, String>,
    val inputs: List<ActionResource>,
    val outputRequests: List<ActionOutputRequest>,
    val command: List<String>,
    val features: ActionFeatures,
) {
    companion object {
        /**
         * Convert a [RunActionRequest] to a [ActionConfiguration].
         *
         * @param request The request to convert.
         * @param config The configuration containing the minimum values the limits must take
         *
         * @return The converted configuration.
         */
        fun fromProto(request: RunActionRequest, config: ResourceLimitsConfig) = ActionConfiguration(
            image = request.environment,
            executionLimits = ActionExecutionLimits.fromProto(request.limits, config),
            envVars = request.envVarsMap,
            inputs = request.inputsList.map { ActionResource.fromProto(it) },
            outputRequests = request.outputRequestsList.map { ActionOutputRequest.fromProto(it) },
            command = request.commandList,
            features = ActionFeatures(
                publicNetworkAccess = request.features.publicNetworkAccess,
            )
        )
    }
}
