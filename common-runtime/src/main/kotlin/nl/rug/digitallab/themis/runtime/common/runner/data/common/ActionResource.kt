package nl.rug.digitallab.themis.runtime.common.runner.data.common

import com.google.protobuf.ByteString
import nl.rug.protospec.judgement.v1.resource
import nl.rug.protospec.judgement.v1.Resource
import nl.rug.protospec.judgement.v1.extensions.uri
import java.net.URI

/**
 * An implementation of a resource that is to be inserted or retrieved from an action.
 *
 * @param uri The URI uniquely identifying the resource.
 * @param content The content of the resource.
 * @param metadata The metadata of the resource (permissions and ids).
 */
data class ActionResource(
    val uri: URI,
    val content: ByteArray,
    val metadata: ActionResourceMetadata,
) {
    /**
     * Convert this object to its protobuf spec equivalent.
     *
     * @return The converted protobuf resource.
     */
    fun toProto(): Resource = resource {
        uri = this@ActionResource.uri
        contents = ByteString.copyFrom(content)
        meta = metadata.toProto()
    }

    companion object {
        /**
         * Convert a protobuf [Resource] to a [ActionResource].
         *
         * @param resource The gRPC proto resource to convert.
         *
         * @return The converted file.
         */
        fun fromProto(resource: Resource) = ActionResource(
            uri = resource.uri,
            content = resource.contents.toByteArray(),
            metadata = ActionResourceMetadata.fromProto(resource.meta),
        )

        /**
         * Creates an empty resource.
         *
         * @param uri The URI that the resource should be identified by.
         *
         * @return The empty resource.
         */
        fun createEmptyResource(uri: URI) = ActionResource(
            uri = uri,
            content = ByteArray(0),
            metadata = ActionResourceMetadata.createEmptyMetadata(),
        )
    }
}
