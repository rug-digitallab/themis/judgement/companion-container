package nl.rug.digitallab.themis.runtime.common.runner.data.request

import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.protospec.judgement.v1.OutputRequest
import nl.rug.protospec.judgement.v1.extensions.maxSize
import nl.rug.protospec.judgement.v1.extensions.uri
import java.net.URI

/**
 * A request for a resource to retrieve from the action after execution.
 *
 * @param uri The URI of the file to retrieve.
 * @param maxSize The maximum size of the file to retrieve.
 */
data class ActionOutputRequest(
    val uri: URI,
    val maxSize: ByteSize,
) {
    companion object {
        /**
         * Convert an [OutputRequest] to a [ActionOutputRequest].
         *
         * @param outputRequest The request to convert.
         *
         * @return The converted request.
         */
        fun fromProto(outputRequest: OutputRequest): ActionOutputRequest = ActionOutputRequest(
            uri = outputRequest.uri,
            maxSize = outputRequest.maxSize,
        )
    }
}
