package nl.rug.digitallab.themis.runtime.common.runner.data.request

/**
 * Enabled features for the action that is being executed.
 *
 * @param publicNetworkAccess Whether the action has access to the public network.
 */
data class ActionFeatures(
    val publicNetworkAccess: Boolean,
)
