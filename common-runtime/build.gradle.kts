plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.project")
}

dependencies {
    val communicationSpecVersion: String by project
    val commonQuarkusVersion: String by project
    val instancioVersion: String by project

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")
    implementation("nl.rug.digitallab.common.quarkus:opentelemetry:$commonQuarkusVersion")

    testImplementation("org.instancio:instancio-core:$instancioVersion")
}
