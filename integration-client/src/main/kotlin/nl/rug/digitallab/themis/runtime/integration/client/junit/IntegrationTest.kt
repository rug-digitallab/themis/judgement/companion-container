package nl.rug.digitallab.themis.runtime.integration.client.junit

import nl.rug.digitallab.themis.runtime.integration.client.grpc.GrpcClient
import nl.rug.digitallab.themis.runtime.integration.client.testcase.data.Testcase
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.assertDoesNotThrow

/**
 * This function contains the code that will run for an integration test.
 *
 * It will first execute the action against the runtime.
 * Then it will compare the output with the expected output of the testcase.
 *
 * @param testcase The testcase to run.
 * @param client The client to execute the action against.
 */
fun integrationTest(testcase: Testcase, client: GrpcClient) {
    val response = assertDoesNotThrow { client.callRuntime(testcase.input) }

    // Alternative outputs are a temporary workaround for differences in environments.
    // An example is Kubernetes, where Docker-in-Kubernetes behaves differently for OOM than Docker-in-Docker or native Docker.
    val correctAlternativeOutput = testcase.alternativeOutputs.any { it.result == response.result }

    // If none of the alternative outputs apply, we check the main output
    // Since assertEquals only runs on this testcase, if the testcase fails, this is the diff shown in the test report.
    if (!correctAlternativeOutput)
        assertEquals(testcase.expectedOutput.result, response.result)

    assertEquals(testcase.expectedOutput.metrics.diskBytes, response.metrics.diskBytes)
    assertEquals(testcase.expectedOutput.metrics.outputBytes, response.metrics.outputBytes)
    assertEquals(testcase.expectedOutput.metrics.createdProcesses, response.metrics.createdProcesses)
    assertEquals(testcase.expectedOutput.metrics.maxProcesses, response.metrics.maxProcesses)
}
