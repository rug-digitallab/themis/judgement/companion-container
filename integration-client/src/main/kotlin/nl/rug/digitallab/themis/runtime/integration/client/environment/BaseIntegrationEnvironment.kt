package nl.rug.digitallab.themis.runtime.integration.client.environment

/**
 * An integration environment is a container hosting the runtime to run the integration tests against.
 * Notably this should not contain the gRPC client for the integration tests. In many cases, it is sufficient for this to simply be a production-grade runtime container.
 * However, you may want to add additional layers such as with the container runtime for infrastructure.
 */
interface BaseIntegrationEnvironment {
    /**
     * The implementation of this function should start a container hosting the runtime to run the integration tests against.
     * This container should publicly expose a gRPC port for the integration tests to make requests to.
     *
     * @return A map of environment variables to be passed to the Quarkus test runner. Contains a host and port for the integration environment.
     */
    fun start(): Map<String, String>

    /**
     * The implementation of this function should stop the container hosting the runtime to run the integration tests against.
     */
    fun stop()
}
