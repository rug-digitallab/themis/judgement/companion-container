package nl.rug.digitallab.themis.runtime.integration.client.testcase.jackson

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.node.JsonNodeType
import com.fasterxml.jackson.databind.node.LongNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.databind.node.TextNode
import nl.rug.digitallab.common.kotlin.quantities.ByteSize
import nl.rug.digitallab.common.kotlin.quantities.exceptions.ByteSizeFormatException
import java.nio.file.Path
import kotlin.io.encoding.Base64
import kotlin.io.encoding.ExperimentalEncodingApi
import kotlin.io.path.exists
import kotlin.io.path.readBytes

/**
 * A parser for the testcase yaml that processes the nodes in the yaml
 * Its main goal is to add extra features that simplify the writing of the test specification
 */
class TestCaseParser(private val directory: Path) {
    /**
     * Walks the tree of the testcase yaml and processes each node
     *
     * @param json The root node of the testcase yaml
     *
     * @return The root node of the testcase yaml with all nodes processed
     */
    fun walkTree(json: JsonNode): JsonNode {
        return when(json.nodeType) {
            JsonNodeType.STRING -> visitString(json)
            JsonNodeType.OBJECT -> visitObject(json.withObject(""))
            JsonNodeType.ARRAY -> visitArray(json.withArray(""))
            else -> json
        }
    }

    /**
     * Replaces a file reference in the testcase yaml with the actual file content
     * This can be used like so: `field: @filename`
     * where filename is the path to the file relative to the testcase yaml's parent folder
     *
     * @param fieldValue The content of the field
     *
     * @return The content of the file as a base64 encoded string
     */
    @OptIn(ExperimentalEncodingApi::class)
    private fun processFileReference(fieldValue: String): String {
        val filename = fieldValue.removePrefix("@")
        val file = directory.resolve(filename)
        require(file.exists()) { "File $filename does not exist" }
        return Base64.encode(file.readBytes())
    }

    /**
     * Visits each string in the testcase yaml and processes them
     * This processing is used to implement new features into the test specification not supported by a simple direct mapping
     *
     * Supports:
     * - File references, allows you to externally write content so that you don't have to put them directly in the yaml
     * - ByteSize library, allows you to specify sizes in a more human-readable format
     */
    private fun visitString(json: JsonNode): JsonNode {
        val content = json.asText()

        // File reference, load in the file content into the field
        if (content.startsWith("@"))
            return TextNode(processFileReference(content))

        // Strings specified using the ByteSize library, convert them to raw bytes as a long
        try {
            ByteSize.fromString(content)
            return LongNode(ByteSize.fromString(content).B)
        } catch (e: ByteSizeFormatException) {
            // Not a ByteSize, ignore
        }

        // No special string, just return the node
        return json
    }

    /**
     * Visits each object in the testcase yaml and processes them
     * Does not do anything special, just walks the tree
     *
     * @param json The object to process
     *
     * @return The processed object
     */
    private fun visitObject(json: ObjectNode): JsonNode {
        json.fieldNames().forEach { json.replace(it, walkTree(json[it])) }
        return json
    }

    /**
     * Visits each array in the testcase yaml and processes them
     * Does not do anything special, just walks the tree
     *
     * @param json The array to process
     *
     * @return The processed array
     */
    private fun visitArray(json: ArrayNode): JsonNode {
        for (i in 0..<json.size())
            json[i] = walkTree(json[i])

        return json
    }
}
