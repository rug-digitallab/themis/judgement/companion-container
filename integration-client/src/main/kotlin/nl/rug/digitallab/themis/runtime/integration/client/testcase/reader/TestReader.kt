package nl.rug.digitallab.themis.runtime.integration.client.testcase.reader

import com.fasterxml.jackson.dataformat.yaml.YAMLMapper
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.integration.client.testcase.data.Testcase
import nl.rug.digitallab.themis.runtime.integration.client.testcase.jackson.TestCaseParser
import org.eclipse.microprofile.config.inject.ConfigProperty
import java.io.File
import kotlin.io.path.Path
import kotlin.io.path.exists
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.name

/**
 * Utility class for reading all the testcases and parsing them.
 */
@ApplicationScoped
class TestReader {
    @ConfigProperty(name = "digital-lab.integration-tester.tests.dir")
    private lateinit var testsDir: String

    @Inject
    private lateinit var mapper: YAMLMapper

    /**
     * Reads all the testcases from the `tests` directory.
     *
     * @return A list of all the testcases.
     */
    fun readAllTests(): List<Testcase> {
        val testsRoot = Path(File(testsDir).path)

        return testsRoot
            .listDirectoryEntries()
            .map { it.resolve("testcase.yaml") }
            .filter { it.exists() }
            .map {
                var specTree = mapper.readTree(it.toFile())
                specTree = TestCaseParser(it.parent).walkTree(specTree)

                val spec = mapper.treeToValue(specTree, Testcase::class.java)
                return@map spec.copy(name = it.parent.name)
            }
    }
}
