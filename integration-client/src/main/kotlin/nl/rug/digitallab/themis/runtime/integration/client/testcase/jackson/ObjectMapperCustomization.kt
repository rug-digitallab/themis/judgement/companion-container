package nl.rug.digitallab.themis.runtime.integration.client.testcase.jackson

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.hubspot.jackson.datatype.protobuf.ProtobufJacksonConfig
import com.hubspot.jackson.datatype.protobuf.ProtobufModule
import io.quarkus.jackson.ObjectMapperCustomizer
import jakarta.inject.Singleton

/**
 * An implementation of [ObjectMapperCustomizer] to add support
 * for parsing Protobuf files
 *
 * This class is automatically used and injected by Jakarta and Quarkus
 */
@Singleton
class ObjectMapperCustomization : ObjectMapperCustomizer {
    override fun customize(objectMapper: ObjectMapper?) {
        // Since we implement a Java signature, we need to check the
        // nullability explicitly, even though this should never happen
        // in practice.
        if (objectMapper == null) return

        // Register Kotlin support
        objectMapper.registerKotlinModule()

        // Configure and register Protobuf support
        val protoMapperConfig = ProtobufJacksonConfig.builder()
            .acceptLiteralFieldnames(true)
            .build()

        objectMapper.registerModule(ProtobufModule(protoMapperConfig))
    }
}
