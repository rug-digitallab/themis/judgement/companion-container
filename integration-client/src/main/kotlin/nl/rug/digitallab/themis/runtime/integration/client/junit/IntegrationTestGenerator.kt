package nl.rug.digitallab.themis.runtime.integration.client.junit

import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.digitallab.themis.runtime.integration.client.grpc.GrpcClient
import nl.rug.digitallab.themis.runtime.integration.client.testcase.reader.TestReader
import org.junit.jupiter.api.DynamicTest

/**
 * Generates the JUnit tests that run the integration tests for a Themis runtime.
 *
 * These tests should be run in the test suite of the runtime. One can do this by adding the following code to the test suite:
 * ```
 * @Inject
 * private lateinit var generator: IntegrationTestGenerator
 *
 * @TestFactory
 * fun spawnIntegrationTests(): List<DynamicTest> = generator.generateIntegrationTests()
 * ```
 *
 * This will generate 1 JUnit test per integration test.
 */
@ApplicationScoped
class IntegrationTestGenerator {
    @Inject
    private lateinit var grpcClient: GrpcClient

    @Inject
    private lateinit var testReader: TestReader

    /**
     * Generates the integration tests.
     *
     * @return A list of dynamic tests to run. The list will contain 1 JUnit test per integration test.
     */
    fun generateIntegrationTests(): List<DynamicTest> {
        val tests = testReader.readAllTests()

        return tests.map { testcase ->
            DynamicTest.dynamicTest("IntegrationTest-${testcase.name}") {
                integrationTest(testcase, grpcClient)
            }
        }
    }
}
