package nl.rug.digitallab.themis.runtime.integration.client.grpc

import io.grpc.ManagedChannelBuilder
import io.grpc.Status
import io.grpc.StatusRuntimeException
import io.quarkus.runtime.annotations.StaticInitSafe
import io.smallrye.config.ConfigMapping
import jakarta.enterprise.context.ApplicationScoped
import jakarta.inject.Inject
import nl.rug.protospec.judgement.v1.ActionServiceGrpc
import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse
import org.jboss.logging.Logger

/**
 * The configuration for the gRPC client.
 *
 * @property host The host of the gRPC server.
 * @property port The port of the gRPC server.
 */
@StaticInitSafe
@ConfigMapping(prefix = "digital-lab.integration-tester.grpc")
interface GrpcConfig {
    fun host(): String
    fun port(): Int
    fun maxRetries(): Int
    fun retryDelay(): Long
}

/**
 * The gRPC client for sending the RunAction request to the judgement manager.
 */
@ApplicationScoped
class GrpcClient {
    @Inject
    private lateinit var config: GrpcConfig

    @Inject
    private lateinit var logger: Logger

    /**
     * Sends the RunAction request to the judgement manager.
     *
     * @param request The request to send.
     *
     * @return The response from the judgement manager.
     */
    fun callRuntime(request: RunActionRequest): RunActionResponse {
        val channel = ManagedChannelBuilder.forAddress(config.host(), config.port())
            .usePlaintext()
            .build()

        try {
            val stub = ActionServiceGrpc.newBlockingStub(channel)

            for(i in 0..config.maxRetries()) {
                try {
                    return stub.runAction(request)
                } catch (e: StatusRuntimeException) {
                    // The execution failed for another reason than the container not being up. So just fail.
                    if (e.status.code != Status.Code.UNAVAILABLE)
                        throw e

                    // The execution failed due to the container not being up. So retry.
                    logger.warn("Failed to send gRPC call to the container runtime, it may not be up yet, retrying...")
                    Thread.sleep(config.retryDelay())
                }
            }

            // Try it one final time outside a try-catch block
            // so that the exception is thrown through the call chain if it still fails.
            return stub.runAction(request)
        } finally {
            channel.shutdown()
        }
    }
}
