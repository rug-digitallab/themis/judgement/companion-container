package nl.rug.digitallab.themis.runtime.integration.client.environment

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager
import org.eclipse.microprofile.config.ConfigProvider

/**
 * Base class for the Quarkus test resource that will spawn the container to run integration tests against.
 * You need to override the `environments` property to provide a map of environments that the runtime's integration tests supports.
 */
abstract class IntegrationEnvironmentSpawner : QuarkusTestResourceLifecycleManager {
    protected abstract val environments: Map<String, BaseIntegrationEnvironment>

    /**
     * Called by Quarkus at the start of the resource's lifecycle.
     * Spins up a container hosting the integration environment used for the integration tests.
     *
     * @return A map of environment variables to be passed to the Quarkus test runner. Contains a host and port for the integration environment.
     */
    override fun start(): Map<String, String> {
        return getTestEnvironment().start()
    }

    /**
     * Called by Quarkus at the end of the resource's lifecycle.
     * Stops the container hosting the integration environment used for the integration tests.
     */
    override fun stop() {
        getTestEnvironment().stop()
    }

    /**
     * Retrieves the environment to run the integration tests against.
     * It first looks in the application config at the key `digital-lab.integration-tester.environment`.
     * It will then retrieve the environment from the `environments` map provided by the specific runtime override.
     *
     * @return The environment to run the integration tests against.
     */
    private fun getTestEnvironment(): BaseIntegrationEnvironment {
        val configKey = ConfigProvider.getConfig()
            .getValue("digital-lab.integration-tester.environment", String::class.java)

        return environments[configKey] ?: throw IllegalArgumentException("Unknown environment: $configKey")
    }
}
