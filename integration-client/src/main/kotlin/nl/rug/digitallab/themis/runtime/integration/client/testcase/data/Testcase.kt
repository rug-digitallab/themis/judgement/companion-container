package nl.rug.digitallab.themis.runtime.integration.client.testcase.data

import nl.rug.protospec.judgement.v1.RunActionRequest
import nl.rug.protospec.judgement.v1.RunActionResponse

/**
 * A testcase, containing the data for the test.
 *
 * @property name The name of the testcase.
 * @property input The request to send over gRPC to the container runtime.
 * @property expectedOutput The expected response from the container runtime.
 * @property alternativeOutputs A list of alternative outputs that are also accepted.
 *                              Introduced to work around Docker-in-Kubernetes behaving different for MemoryExceeded
 */
data class Testcase (
    val name: String?,
    val input: RunActionRequest,
    val expectedOutput: RunActionResponse,
    val alternativeOutputs: List<RunActionResponse> = emptyList()
)
