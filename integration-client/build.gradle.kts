plugins {
    id("nl.rug.digitallab.gradle.plugin.quarkus.library")
}

dependencies {
    val communicationSpecVersion: String by project
    val jacksonProtobufVersion: String by project

    // Quarkus dependencies
    implementation("io.quarkus:quarkus-jackson")
    implementation("io.quarkus:quarkus-test-common")

    // Digital Lab dependencies
    implementation("nl.rug.digitallab.themis.judgement:communication-spec:$communicationSpecVersion")

    // Jackson
    implementation("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("com.hubspot.jackson:jackson-datatype-protobuf:$jacksonProtobufVersion")
}
