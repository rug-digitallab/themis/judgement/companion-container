rootProject.name = "kotlin-runtimes"

pluginManagement {
    val digitalLabGradlePluginVersion: String by settings

    plugins {
        id("nl.rug.digitallab.gradle.plugin.quarkus.project") version digitalLabGradlePluginVersion
        id("nl.rug.digitallab.gradle.plugin.quarkus.library") version digitalLabGradlePluginVersion
    }

    repositories {
        maven("https://gitlab.com/api/v4/groups/65954571/-/packages/maven") // Digital Lab Group Repository
        gradlePluginPortal()
    }
}

include("common-runtime")

include("container-runtime:process-monitor")
include("container-runtime:runtime")
include("container-runtime:integration-tests")

include("executable-runtime:runtime")
include("executable-runtime:integration-tests")

include("integration-client")
